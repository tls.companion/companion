<?php

namespace App\Repository\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Speech;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Speech>
 *
 * @method Speech|null find($id, $lockMode = null, $lockVersion = null)
 * @method Speech|null findOneBy(array $criteria, array $orderBy = null)
 * @method Speech[]    findAll()
 * @method Speech[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpeechRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Speech::class);
    }

    public function add(Speech $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Speech $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findFormForm()
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.weekends', 'w')
            ->orderBy('w.date', 'ASC')
            ;
    }

    public function findAllForPaginator()
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.weekends', 'w')
            ->addSelect('w')
            ->orderBy('s.number', 'ASC')
            ->getQuery();
    }

    public function findLastTime($id)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.weekends', 'w')
            ->select('MAX(w.date) AS date')
            ->where('s.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function autocomplete(?string $term = null)
    {
        $qb = $this->createQueryBuilder('s')
            ->select('s')
            ->leftJoin('s.weekends', 'w')
            ->leftJoin('s.category', 'c')
            ;


        if ($term) {
            $qb
                ->where('s.number = :number')
                ->orWhere('s.title LIKE :term')
                ->setParameters([
                    'number' => $term,
                    'term' => '%' . $term . '%'
                ]);
        }

        return $qb
            ->andWhere('s.deletedAt IS NULL')
            ->getQuery()
            ->getResult();
    }

    public function findNotGiven()
    {
        $em = $this->getEntityManager();
        $expr = $em->getExpressionBuilder();
        return $this->createQueryBuilder('s')
            ->select('s')
            ->leftJoin('s.weekends', 'w')
            ->where($expr->isNull('w.id'))
            ->andWhere('s.deletedAt IS NULL')
            ->getQuery()
            ->getResult();
    }

  public function findPicked()
  {
    return $this->createQueryBuilder('s')
      ->leftJoin('s.speechSurveys', 'speech_surveys')
      ->where('speech_surveys.id IS NOT NULL')
      ->getQuery()
      ->getResult()
      ;
  }
}
