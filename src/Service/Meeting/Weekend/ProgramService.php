<?php

namespace App\Service\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Program;
use App\Entity\Meeting\Weekend\Weekend;
use App\Entity\User;
use App\Repository\Meeting\Weekend\ProgramRepository;
use App\Service\Utils\Meeting\Weekend\ProgramUtils;

class ProgramService
{
    private ProgramRepository $repository;
    private ProgramUtils $utils;

    public function __construct(ProgramRepository $repository, ProgramUtils $utils)
    {
        $this->repository = $repository;
        $this->utils = $utils;
    }


    public function createOrUpdate(Program $program, ?User $user = null): Program
    {
        $program = $this->utils->resetUnusedField($program);
        if ($user) {

            $program->setCreatedBy($user);
        }
        $this->repository->add($program, true);
        return $program;
    }

    public function prepareCreate(int $month, int $year): Program
    {
        $program = new Program();
        $weekends = $this->utils->getWeekendsPeriod($month, $year);

        foreach ($weekends as $weekend) {
            $tmpWeekend = new Weekend();
            $tmpWeekend
                ->setDate($weekend);
            $program->addWeekend($tmpWeekend);
        }
        $program->setStartDate($weekends->start);
        $program->setEndDate($weekends->end);

        return $program;
    }

}
