<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230918154623 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE wishlist (id INT AUTO_INCREMENT NOT NULL, publisher_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_9CE12A3140C86FCE (publisher_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE wishlist_speech (wishlist_id INT NOT NULL, speech_id INT NOT NULL, INDEX IDX_39C93EB1FB8E54CD (wishlist_id), INDEX IDX_39C93EB1BBC049D6 (speech_id), PRIMARY KEY(wishlist_id, speech_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE wishlist ADD CONSTRAINT FK_9CE12A3140C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE wishlist_speech ADD CONSTRAINT FK_39C93EB1FB8E54CD FOREIGN KEY (wishlist_id) REFERENCES wishlist (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE wishlist_speech ADD CONSTRAINT FK_39C93EB1BBC049D6 FOREIGN KEY (speech_id) REFERENCES speech (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE publisher ADD wishlist_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE publisher ADD CONSTRAINT FK_9CE8D546FB8E54CD FOREIGN KEY (wishlist_id) REFERENCES wishlist (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9CE8D546FB8E54CD ON publisher (wishlist_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE publisher DROP FOREIGN KEY FK_9CE8D546FB8E54CD');
        $this->addSql('ALTER TABLE wishlist DROP FOREIGN KEY FK_9CE12A3140C86FCE');
        $this->addSql('ALTER TABLE wishlist_speech DROP FOREIGN KEY FK_39C93EB1FB8E54CD');
        $this->addSql('ALTER TABLE wishlist_speech DROP FOREIGN KEY FK_39C93EB1BBC049D6');
        $this->addSql('DROP TABLE wishlist');
        $this->addSql('DROP TABLE wishlist_speech');
        $this->addSql('DROP INDEX UNIQ_9CE8D546FB8E54CD ON publisher');
        $this->addSql('ALTER TABLE publisher DROP wishlist_id');
    }
}
