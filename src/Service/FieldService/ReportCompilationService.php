<?php

namespace App\Service\FieldService;

use App\Entity\FieldService\Report;
use App\Entity\FieldService\ReportCompilation;
use App\Entity\FieldService\ServiceGroup;
use App\Entity\User;
use App\Repository\FieldService\ReportCompilationRepository;
use App\Repository\FieldService\ServiceGroupRepository;

class ReportCompilationService
{

    private ServiceGroupRepository $groupRepository;
    private ReportCompilationRepository $compilationRepository;

    /**
     * @param ServiceGroupRepository $groupRepository
     * @param ReportCompilationRepository $compilationRepository
     */
    public function __construct(ServiceGroupRepository $groupRepository, ReportCompilationRepository $compilationRepository)
    {
        $this->groupRepository = $groupRepository;
        $this->compilationRepository = $compilationRepository;
    }


    public function prepareCreate(\DateTime $date, ServiceGroup $group, User $user): ReportCompilation
    {
        $reportCompilation = $this->compilationRepository->findOneBy(['date' => $date, 'serviceGroup' => $group]);

        if (!$reportCompilation) {
            $reportCompilation = new ReportCompilation();
            $reportCompilation
                ->setDate($date)
                ->setServiceGroup($group)
            ;

            foreach ($group->getPublishers() as $publisher) {
                $report = new Report();
                $report
                    ->setDate($date)
                    ->setCreatedBy($user)
                    ->setPublisher($publisher);
                $reportCompilation->addReport($report);
            }
        }

        return $reportCompilation;
    }

    public function createOrUpdate(ReportCompilation $reportCompilation): ReportCompilation
    {
        $this->compilationRepository->add($reportCompilation, true);
        return $reportCompilation;
    }

}
