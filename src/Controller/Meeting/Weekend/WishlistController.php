<?php

namespace App\Controller\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Wishlist;
use App\Form\Meeting\Weekend\EditWishlistType;
use App\Form\Meeting\Weekend\WishlistType;
use App\Repository\Meeting\Weekend\WishlistRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/meeting/weekend/wishlists')]
class WishlistController extends AbstractController
{
    private WishlistRepository $repository;


    public function __construct(WishlistRepository $repository)
    {
        $this->repository = $repository;
    }

    #[Route(path: '/', name: 'app_meeting_weekend_wishlist_list')]
    public function list(): Response
    {
        return $this->render('meeting/wishlist/list.html.twig', [
            'wishlists' => $this->repository->findAll()
        ]);
    }

    #[Route(path: '/create', name: 'app_meeting_weekend_wishlist_create')]
    public function create(Request $request): Response
    {
        $wishlist = new Wishlist();
        $form = $this->createForm(WishlistType::class, $wishlist);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->repository->add($wishlist, true);
            $this->addFlash('success', "Liste de discours ajouté pour {$wishlist->getPublisher()->getFirstName()}" );
            if ($request->query->get('createNext')) {
                return $this->redirectToRoute('app_meeting_weekend_wishlist_create');
            }
            return  $this->redirectToRoute('app_meeting_weekend_wishlist_list');
        }

        return $this->render('meeting/wishlist/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_meeting_weekend_wishlist_edit')]
    public function edit(Wishlist $wishlist, Request $request): Response
    {
        $form = $this->createForm(EditWishlistType::class, $wishlist);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->repository->add($wishlist, true);
            $this->addFlash('success', "Liste de discours modifiée pour {$wishlist->getPublisher()->getFirstName()}" );
            return  $this->redirectToRoute('app_meeting_weekend_wishlist_list');
        }

        return $this->render('meeting/wishlist/edit.html.twig', [
            'form' => $form->createView(),
            'wishlist' => $wishlist
        ]);
    }

}