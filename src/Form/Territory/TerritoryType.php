<?php

namespace App\Form\Territory;

use App\Entity\Territory\Area;
use App\Entity\Territory\Territory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TerritoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre'
            ])
            ->add('cloudLink', TextType::class, [
                'required' => false,
                'label' => 'Lien'
            ])
            ->add('area', EntityType::class, [
                'class' => Area::class,
                'choice_label' => 'title',
                'label' => 'Zone',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Territory::class,
        ]);
    }
}
