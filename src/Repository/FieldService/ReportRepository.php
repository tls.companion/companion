<?php

namespace App\Repository\FieldService;

use App\Entity\FieldService\Report;
use App\Entity\FieldService\ServiceGroup;
use App\Entity\Publisher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @extends ServiceEntityRepository<Report>
 *
 * @method Report|null find($id, $lockMode = null, $lockVersion = null)
 * @method Report|null findOneBy(array $criteria, array $orderBy = null)
 * @method Report[]    findAll()
 * @method Report[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportRepository extends ServiceEntityRepository
{
    protected TokenStorageInterface $tokenStorage;

    public function __construct(ManagerRegistry $registry, TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        parent::__construct($registry, Report::class);
    }

    public function add(Report $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Report $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function checkNextMonthAvailable(ServiceGroup $group)
    {
        $currentMonth = new \DateTime("first day of now");
        $qb = $this->createQueryBuilder('r')
            ->where('r.date = :currentMonth')
            ->setParameters([
                'currentMonth' => $currentMonth
            ])
            ->getQuery()
            ->getResult();



        if (sizeof($qb) > 0) {
            return new \DateTime("first day of next month");
        }

        return $currentMonth;

    }

    public function findAllByGroup(int $id)
    {
        $results = $this->createQueryBuilder('r')
            ->leftJoin('r.publisher', 'p')
            ->leftJoin('p.serviceGroup', 'sg')
            ->where('sg.id = :id')
            ->setParameters([
                'id' => $id,
            ])
            ->orderBy('r.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;

        if ($results) {
            $reports = [];
            foreach ($results as $result) {
                if (!isset($reports[$result->getDate()->format('d/m/Y')])) {
                    $reports[$result->getDate()->format('d/m/Y')] = [];
                }
                array_push($reports[$result->getDate()->format('d/m/Y')], $result);
            }
            return $reports;
        }

        return $results;
    }

    public function findOrCreate(\DateTime $date, Publisher $publisher)
    {
        $result = $this->createQueryBuilder('r')
            ->leftJoin('r.publisher', 'p')
            ->where('r.date = :date')
            ->andWhere('p.id = :idPublisher' )
            ->setParameters([
                'date' => $date,
                'idPublisher' => $publisher->getId()
            ])
            ->getQuery()
            ->getOneOrNullResult();

        if (!$result) {
            $report = new Report();
            $report
                ->setPublisher($publisher)
                ->setDate($date)
                ->setCreatedBy($this->tokenStorage->getToken()->getUser())
            ;
            return $report;
        }

        return $result;

    }
}
