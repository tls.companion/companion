<?php

namespace App\Exception;


class UserNotGrantedException extends \RuntimeException
{
    public function __construct($message = "", $code = 0, \Throwable $previous = null)
    {
        parent::__construct("Tu n'es pas autorisé à acceder à cette page. Merci de contacter le secrétaire de l'assemblée pour plus d'informations.", $code, $previous);
    }

}
