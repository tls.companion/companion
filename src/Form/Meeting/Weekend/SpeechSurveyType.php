<?php

namespace App\Form\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Speech;
use App\Entity\Meeting\Weekend\SpeechSurvey;
use App\Entity\Publisher;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpeechSurveyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('observations', TextareaType::class, [
              'label' => 'Observations',
              'required' => false
            ])
            ->add('speeches', EntityType::class, [
              'class' => Speech::class,
              'multiple' => true,
              'label' => 'Thèmes discours publics (plusieurs choix possibles)',
              'attr' => [
              ],
              'choice_label' => 'forSurvey',
              'expanded' => true,
              'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('s')
                  ->andWhere('s.presentationReady = :presentationReady')
                  ->setParameter('presentationReady', true);
              },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SpeechSurvey::class,
        ]);
    }
}
