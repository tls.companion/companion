<?php

namespace App\Entity\Meeting\Weekend;

use App\Repository\Meeting\Weekend\WeekendCategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: WeekendCategoryRepository::class)]
class WeekendCategory
{
    #[Groups(['show'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['show'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $name;

    #[Groups(['show'])]
    #[ORM\Column(type: 'boolean')]
    private $chairmanEnabled;

    #[Groups(['show'])]
    #[ORM\Column(type: 'boolean')]
    private $readerEnabled;

    #[Groups(['show'])]
    #[ORM\Column(type: 'boolean')]
    private $speakerEnabled;

    #[Groups(['show'])]
    #[ORM\Column(type: 'boolean')]
    private $externalSpeakerEnabled;

    #[Groups(['show'])]
    #[ORM\Column(type: 'boolean')]
    private $speechEnabled;

    #[ORM\OneToMany(targetEntity: Weekend::class, mappedBy: 'category')]
    private $weekends;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $conventionEnabled;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private $overseerVisitEnabled;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private $deletedAt;

    public function __construct()
    {
        $this->weekends = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function isChairmanEnabled(): ?bool
    {
        return $this->chairmanEnabled;
    }

    public function setChairmanEnabled(bool $chairmanEnabled): self
    {
        $this->chairmanEnabled = $chairmanEnabled;

        return $this;
    }

    public function isReaderEnabled(): ?bool
    {
        return $this->readerEnabled;
    }

    public function setReaderEnabled(bool $readerEnabled): self
    {
        $this->readerEnabled = $readerEnabled;

        return $this;
    }

    public function isSpeakerEnabled(): ?bool
    {
        return $this->speakerEnabled;
    }

    public function setSpeakerEnabled(bool $speakerEnabled): self
    {
        $this->speakerEnabled = $speakerEnabled;

        return $this;
    }

    public function isExternalSpeakerEnabled(): ?bool
    {
        return $this->externalSpeakerEnabled;
    }

    public function setExternalSpeakerEnabled(bool $externalSpeakerEnabled): self
    {
        $this->externalSpeakerEnabled = $externalSpeakerEnabled;

        return $this;
    }

    public function isSpeechEnabled(): ?bool
    {
        return $this->speechEnabled;
    }

    public function setSpeechEnabled(bool $speechEnabled): self
    {
        $this->speechEnabled = $speechEnabled;

        return $this;
    }

    /**
     * @return Collection<int, Weekend>
     */
    public function getWeekends(): Collection
    {
        return $this->weekends;
    }

    public function addWeekend(Weekend $weekend): self
    {
        if (!$this->weekends->contains($weekend)) {
            $this->weekends[] = $weekend;
            $weekend->setCategory($this);
        }

        return $this;
    }

    public function removeWeekend(Weekend $weekend): self
    {
        if ($this->weekends->removeElement($weekend)) {
            // set the owning side to null (unless already changed)
            if ($weekend->getCategory() === $this) {
                $weekend->setCategory(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function isConventionEnabled(): ?bool
    {
        return $this->conventionEnabled;
    }

    public function setConventionEnabled(?bool $conventionEnabled): self
    {
        $this->conventionEnabled = $conventionEnabled;

        return $this;
    }

    public function isOverseerVisitEnabled(): ?bool
    {
        return $this->overseerVisitEnabled;
    }

    public function setOverseerVisitEnabled(?bool $overseerVisitEnabled): self
    {
        $this->overseerVisitEnabled = $overseerVisitEnabled;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }
}
