<?php

namespace App\Entity\Meeting\Weekend;

use App\Entity\Publisher;
use App\Repository\Meeting\SpeechSurveyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SpeechSurveyRepository::class)]
class SpeechSurvey
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'speechSurveys')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Publisher $speaker = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $observations = null;

    #[ORM\ManyToMany(targetEntity: Speech::class, inversedBy: 'speechSurveys')]
    private Collection $speeches;

    #[ORM\Column(length: 255)]
    private ?string $token = null;

    #[ORM\Column(nullable: true)]
    private ?int $year = null;

    public function __construct()
    {
        $this->speeches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpeaker(): ?Publisher
    {
        return $this->speaker;
    }

    public function setSpeaker(?Publisher $speaker): static
    {
        $this->speaker = $speaker;

        return $this;
    }

    public function getObservations(): ?string
    {
        return $this->observations;
    }

    public function setObservations(?string $observations): static
    {
        $this->observations = $observations;

        return $this;
    }

    /**
     * @return Collection<int, Speech>
     */
    public function getSpeeches(): Collection
    {
        return $this->speeches;
    }

    public function addSpeech(Speech $speech): static
    {
        if (!$this->speeches->contains($speech)) {
            $this->speeches->add($speech);
        }

        return $this;
    }

    public function removeSpeech(Speech $speech): static
    {
        $this->speeches->removeElement($speech);

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): static
    {
        $this->token = $token;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): static
    {
        $this->year = $year;

        return $this;
    }
}
