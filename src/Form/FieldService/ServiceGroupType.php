<?php

namespace App\Form\FieldService;

use App\Entity\FieldService\ServiceGroup;
use App\Entity\Publisher;
use App\Repository\PublisherRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServiceGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'required' => true,
                'label' => 'Nom du groupe',
                'attr' => [
                    'placeholder' => 'ex: Cartoucherie'
                ]
            ])
            ->add('publishers', EntityType::class, [
                'class' => Publisher::class,
                'multiple' => true,
                'required' => false,
                'choice_label' => 'fullName',
                'attr' => [
                    'class' => 'js-select2'
                ],
                'by_reference' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.deletedAt IS NULL');
                }
            ])
            ->add('overseer', EntityType::class, [
                'class' => Publisher::class,
                'required' => true,
                'attr' => [
                    'class' => 'js-select2'
                ],
                'label' => 'Responsable de groupe',
                'query_builder' => function(EntityRepository $er) { /** @var $er PublisherRepository */
                    return $er->createQueryBuilder('p')
                        ->where('p.isElder = 1');
                }

            ])
            ->add('assistant', EntityType::class, [
                'class' => Publisher::class,
                'required' => false,
                'attr' => [
                    'class' => 'js-select2'
                ],
                'label' => 'Adjoint au responsable',
                'query_builder' => function(EntityRepository $er) { /** @var $er PublisherRepository */
                    return $er->createQueryBuilder('p')
                        ->where('p.isElder = 1')
                        ->orWhere('p.isServant = 1');
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ServiceGroup::class
        ]);
    }
}
