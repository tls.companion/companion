<?php

namespace App\Repository\Meeting;

use App\Entity\Meeting\Weekend\SpeechSurvey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SpeechSurvey>
 *
 * @method SpeechSurvey|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpeechSurvey|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpeechSurvey[]    findAll()
 * @method SpeechSurvey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpeechSurveyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpeechSurvey::class);
    }

  public function findAllForList(int $year)
  {
    return $this->createQueryBuilder('s')
      ->leftJoin('s.speaker', 'speaker')
      ->where('s.year = :year')
      ->setParameters([
        'year' => $year
      ])
      ->orderBy('speaker.lastName', 'ASC')
      ->getQuery()
      ->getResult()
      ;
  }
}
