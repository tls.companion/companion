<?php

namespace App\Repository\Territory;

use App\Entity\Territory\Territory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Territory>
 *
 * @method Territory|null find($id, $lockMode = null, $lockVersion = null)
 * @method Territory|null findOneBy(array $criteria, array $orderBy = null)
 * @method Territory[]    findAll()
 * @method Territory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TerritoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Territory::class);
    }

    public function add(Territory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Territory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findTerritoryForShow(int $id)
    {
        return $this->createQueryBuilder('t')
            ->addSelect('h')
            ->leftJoin('t.histories', 'h')
            ->where('t.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult()
            ->sortHistories()
            ;
    }

    public function countTerritoriesWorked()
    {
        $qb = $this->createQueryBuilder('t')
            ->select('t.id')
            ->leftJoin('t.histories', 'h')
            ->where('h.outDate BETWEEN :august AND :september')
            ->andWhere('h.inDate BETWEEN :august AND :september')
            ->setParameters([
                'september' => new \DateTime('2023-09-01'),
                'august' => new \DateTime('2024-08-31')
            ])
            ->distinct()
            ->getQuery()
            ->getResult();

        return count($qb);
    }
}
