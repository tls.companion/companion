<?php

namespace App\Controller\Territory;

use App\Repository\Territory\AreaRepository;
use App\Service\Office\TerritoryAllocationRegister;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/territories/export')]
class ExportController extends AbstractController
{

    #[Route(path: '/synthesis', name: 'app_territory_export_synthesis')]
    public function exportSynthesis(Pdf $pdf, AreaRepository $areaRepository): void
    {
        $areas = $areaRepository->findForExportS13();
        $html = $this->renderView('territory/export/synthesis.html.twig', [
            'areas' => $areas
        ]);

        header('Content-Type: application/pdf');
        echo new PdfResponse(
            $pdf->getOutputFromHtml($html),
            'testsyntehsis' . '.pdf'
        );

    }


  #[Route(path: '/territory/register', name: 'app_territory_export_register')]
    public function allocationRegister(TerritoryAllocationRegister $register): BinaryFileResponse
    {
      $spreadsheet = $register->write();
      $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
      $writer->save('s13.xlsx');

      return new BinaryFileResponse('s13.xlsx');
    }
}
