<?php

namespace App\Form;

use App\Entity\FieldService\ServiceGroup;
use App\Entity\Publisher;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PublisherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', null, [
                'label' => 'Prénom'
            ])
            ->add('lastName', null, [
                'label' => 'Nom de famille'
            ])
            ->add('serviceGroup', EntityType::class, [
                'class' => ServiceGroup::class,
                'label' => 'Groupe de prédication',
                'placeholder' => 'Choix du groupe de prédication',
                'required' => false
            ])
            ->add('isElder', CheckboxType::class, [
                'label' => 'Ancien',
                'required'=> false
            ])
            ->add('isServant', CheckboxType::class, [
                'label' => 'Assistant',
                'required' => false
            ])
            ->add('isRegularPioneer', CheckboxType::class, [
                'label' => 'Pionnier permanent',
                'required' => false
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'Genre',
                'required' => true,
                'choices' => [
                    'Homme' => Publisher::MALE,
                    'Femme' => Publisher::FEMALE
                ],
                'attr' => ['class' => 'js-select2']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Publisher::class,
        ]);
    }
}
