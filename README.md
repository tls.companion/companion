# Companion

## Installation du projet

Prérequis : 
* PHP 7.4
* MySQL 8
* NodeJS
* Yarn
* Composer

Après avoir cloné le projet, se rendre dans le dossier du projet, puis :
```shell
composer install
```
Ensuite, copiez le fichier ``.env`` vers ``.env.local``
Pensez à renseigner les variables suivantes ⚠ :
* **DATABASE_URL**
* **MAILER_DSN** (la clé API sendiblue se trouve sur Confluence)
* **DEFAULT_ADMIN_PASSWORD** _(doit contenir 8 caractères minimum dont au moins 1 caractère spécial et 1 chiffre)_
* **DEFAULT_USER_PASSWORD** _(doit contenir 8 caractères minimum dont au moins 1 caractère spécial et 1 chiffre)_
* **EMAIL_FROM** (vous pouvez mettre [tls.companion@gmail.com]())
* **BASE_URI** : Il s'agit du domaine sur lequel vous lancez l'app. par ex: https://localhost:8000 ou https://tls-companion.fr

Puis vous pouvez lancer la commande suivante pour créer la base de donnée :
```shell 
php bin/console doctrine:database:create
```
Pour charger les tables et migrations : 
```shell
php bin/console doctrine:migrations:migrate
```

Une fois que cela est fait, vous pouvez créer un compte admin (important pour se connecter la première fois)
```shell
php bin/console app:init-admin John Doe "john.doe@companion.io"
```

*Webpack Encore*
Pour installer les dépendances yarn :

```shell
yarn install
```
Puis

```shell
yarn dev
```

Voila ! Si vous n'avez pas créé de VirtualHost, vous pouvez lancer un WebServer Symfony.
Pour cela, assurez-vous d'avoir installé le [Symfony CLI](https://symfony.com/download)

```shell
symfony server:start
```

