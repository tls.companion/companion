<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230215142059 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE report (id INT AUTO_INCREMENT NOT NULL, publisher_id INT NOT NULL, created_by_id INT NOT NULL, report_compilation_id INT DEFAULT NULL, date DATE NOT NULL, times INT DEFAULT NULL, videos INT DEFAULT NULL, visits INT DEFAULT NULL, courses INT DEFAULT NULL, comment LONGTEXT DEFAULT NULL, publications INT DEFAULT NULL, INDEX IDX_C42F778440C86FCE (publisher_id), INDEX IDX_C42F7784B03A8386 (created_by_id), INDEX IDX_C42F7784F574643D (report_compilation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE report_compilation (id INT AUTO_INCREMENT NOT NULL, service_group_id INT DEFAULT NULL, closed TINYINT(1) NOT NULL, INDEX IDX_1C4726FA722827A (service_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F778440C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784B03A8386 FOREIGN KEY (created_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE report ADD CONSTRAINT FK_C42F7784F574643D FOREIGN KEY (report_compilation_id) REFERENCES report_compilation (id)');
        $this->addSql('ALTER TABLE report_compilation ADD CONSTRAINT FK_1C4726FA722827A FOREIGN KEY (service_group_id) REFERENCES service_group (id)');
        $this->addSql('ALTER TABLE user ADD publisher_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D64940C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D64940C86FCE ON user (publisher_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F778440C86FCE');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784B03A8386');
        $this->addSql('ALTER TABLE report DROP FOREIGN KEY FK_C42F7784F574643D');
        $this->addSql('ALTER TABLE report_compilation DROP FOREIGN KEY FK_1C4726FA722827A');
        $this->addSql('DROP TABLE report');
        $this->addSql('DROP TABLE report_compilation');
        $this->addSql('ALTER TABLE `user` DROP FOREIGN KEY FK_8D93D64940C86FCE');
        $this->addSql('DROP INDEX UNIQ_8D93D64940C86FCE ON `user`');
        $this->addSql('ALTER TABLE `user` DROP publisher_id');
    }
}
