<?php

namespace App\Controller\Territory;

use App\Service\Territory\KPIService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/territories')]
class KPIController extends AbstractController
{

    private KPIService $service;

    public function __construct(KPIService $service)
    {
        $this->service = $service;
    }

    #[Route(path: '/kpi', name: 'app_territory_kpi')]
    public function kpi(): Response
    {
        return $this->render('territory/kpi/kpi.html.twig', [
            'kpis' => $this->service->getKPIs(),
            'areas' => $this->service->areasWorkedTotally()
        ]);
    }

}
