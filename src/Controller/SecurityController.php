<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ResetPassowordForgottenType;
use App\Repository\UserRepository;
use App\Service\SecurityService;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    #[Route(path: '/login', name: 'app_login')]
    public function login(AuthenticationUtils $utils, Request $request): Response
    {
        if ($this->getUser()) {
            if ($this->isGranted('ROLE_SERVICE_COMMITTEE')) {
                return $this->redirectToRoute('app_fieldservice_servicegroup_list');
            }
            if ($this->isGranted('ROLE_PUBLIC_TALK_PROGRAMMER')) {
                $year = (new \DateTime())->format('Y');
                return $this->redirectToRoute('app_meeting_weekend_program_list', ['year' => $year]);
            }
            if ($this->isGranted('ROLE_TERRITORY')) {
                return  $this->redirectToRoute('app_territory_areas_list');
            }
            return $this->redirectToRoute('app_fieldservice_servicegroup_list');
        }

        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'error' => $error,
            'lastUsername' => $lastUsername,
        ]);
    }

    #[Route(path: '/logout', name: 'app_logout', methods: ['GET'])]
    public function logout(): void
    {
        // controller can be blank: it will never be called!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    #[Route(path: '/reset/ask', name: 'app_security_askreset')]
    public function askReset(Request $request, UserRepository $repository, MailerInterface $mailer)
    {
        if ($request->isMethod('POST')) {
            $email = $request->request->get('email');
            $user = $repository->findOneBy(['email' => $email]);
            if (!$user) {
                $this->addFlash('error', 'Adresse e-mail non trouvé.');
                return $this->render('security/ask-reset.html.twig');
            }
            $user->setToken(Uuid::uuid4()->toString());
            $repository->add($user, true);
            $email = (new TemplatedEmail())
                ->from($_ENV['EMAIL_FROM'])
                ->to(new Address($user->getEmail()))
                ->subject('Toulouse Companion | Mot de passe oublié')

                // path of the Twig template to render
                ->htmlTemplate('emails/ask-forgot.html.twig')
                ->context([
                    'user' => $user,
                ]);

            $mailer->send($email);

            $this->addFlash('success', 'Un e-mail avec un lien de réinitialisation a été envoyé. Si tu ne le vois pas, vérifie tes spams.');

        }
        return $this->render('security/ask-reset.html.twig');
    }

    #[Route(path: '/reset/password/{token}', name: 'app_security_resetpassword')]
    public function resetPassword(User $user, Request $request, SecurityService $service)
    {
        $form = $this->createForm(ResetPassowordForgottenType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            $service->resetPassword($user);
            $this->addFlash('success', 'Ton mot de passe a été modifié.');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('security/reset-password.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'isActivation' => false
        ]);
    }

    #[Route(path: '/activation/{token}', name: 'app_security_activate')]
    public function activate(User $user, Request $request, SecurityService $service): Response
    {
        $form = $this->createForm(ResetPassowordForgottenType::class, $user);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $service->resetPassword($user, true);
            $this->addFlash('success', 'Ton compte a bien été activé.');
            return $this->redirectToRoute('app_login');
        }
        return $this->render('security/reset-password.html.twig', [
            'form' => $form->createView(),
            'user' => $user,
            'isActivation' => true
        ]);
    }

}
