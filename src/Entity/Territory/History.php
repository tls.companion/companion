<?php

namespace App\Entity\Territory;

use App\Entity\Publisher;
use App\Entity\User;
use App\Repository\Territory\HistoryRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: HistoryRepository::class)]
class History
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'datetime')]
    private $outDate;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private $inDate;

    #[ORM\ManyToOne(targetEntity: Publisher::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $publisher;

    #[ORM\ManyToOne(targetEntity: Territory::class, inversedBy: 'histories')]
    #[ORM\JoinColumn(nullable: false)]
    private $territory;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private $createdBy;

    #[ORM\Column(type: 'datetime_immutable')]
    private $createdAt;

    public function __construct()
    {
        $this->outDate = new \DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOutDate(): ?\DateTimeInterface
    {
        return $this->outDate;
    }

    public function setOutDate(\DateTimeInterface $outDate): self
    {
        $this->outDate = $outDate;

        return $this;
    }

    public function getInDate(): ?\DateTimeInterface
    {
        return $this->inDate;
    }

    public function setInDate(?\DateTimeInterface $inDate): self
    {
        $this->inDate = $inDate;

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getTerritory(): ?Territory
    {
        return $this->territory;
    }

    public function setTerritory(?Territory $territory): self
    {
        $this->territory = $territory;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?User $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
