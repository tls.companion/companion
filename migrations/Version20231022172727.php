<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231022172727 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE board_item ADD category_id INT NOT NULL');
        $this->addSql('ALTER TABLE board_item ADD CONSTRAINT FK_C58D7C3D12469DE2 FOREIGN KEY (category_id) REFERENCES board_category (id)');
        $this->addSql('CREATE INDEX IDX_C58D7C3D12469DE2 ON board_item (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE board_item DROP FOREIGN KEY FK_C58D7C3D12469DE2');
        $this->addSql('DROP INDEX IDX_C58D7C3D12469DE2 ON board_item');
        $this->addSql('ALTER TABLE board_item DROP category_id');
    }
}
