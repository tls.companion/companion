<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class MailerService
{

    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public function send(User $toUser, string $subject, string $template, array $context)
    {
        $email = (new TemplatedEmail())
            ->to(new Address($toUser->getEmail()))
            ->subject($subject)

            // path of the Twig template to render
            ->htmlTemplate($template)
            ->context($context);

        $this->mailer->send($email);
    }

}
