<?php

namespace App\Repository\Meeting\Weekend;

use App\Entity\Meeting\Weekend\WeekendCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<WeekendCategory>
 *
 * @method WeekendCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method WeekendCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method WeekendCategory[]    findAll()
 * @method WeekendCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeekendCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WeekendCategory::class);
    }

    public function add(WeekendCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(WeekendCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllForForm()
    {
        return $this->createQueryBuilder('w')
            ->where('w.deletedAt IS NULL');
    }
}
