<?php

namespace App\Entity;

use App\Entity\FieldService\Report;
use App\Entity\FieldService\ServiceGroup;
use App\Entity\Meeting\Weekend\SpeechSurvey;
use App\Entity\Meeting\Weekend\Weekend;
use App\Entity\Meeting\Weekend\Wishlist;
use App\Repository\PublisherRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PublisherRepository::class)]
class Publisher
{
    const MALE = 'M';
    const FEMALE = 'F';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 128)]
    private ?string $firstName;

    #[ORM\Column(type: 'string', length: 128)]
    private ?string $lastName;

    #[ORM\ManyToOne(targetEntity: ServiceGroup::class, inversedBy: 'publishers')]
    #[ORM\JoinColumn(nullable: true)]
    private ?ServiceGroup $serviceGroup;

    #[ORM\Column(type: 'boolean')]
    private bool $isElder = false;

    #[ORM\Column(type: 'boolean')]
    private bool $isServant = false;

    #[ORM\Column(type: 'boolean')]
    private bool $isRegularPioneer = false;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?\DateTimeImmutable $deletedAt;

    #[ORM\OneToMany(targetEntity: Report::class, mappedBy: 'publisher', orphanRemoval: true)]
    private $reports;

    #[ORM\OneToOne(targetEntity: User::class, mappedBy: 'publisher', cascade: ['persist', 'remove'])]
    private ?User $user;

    #[ORM\OneToOne(targetEntity: Assignment::class, mappedBy: 'publisher', cascade: ['persist', 'remove'])]
    private ?Assignment $assignment;

    #[ORM\Column(type: 'string', length: 1)]
    private string $gender = self::MALE;

    #[ORM\OneToMany(targetEntity: Weekend::class, mappedBy: 'speaker', orphanRemoval: false)]
    private $speakerWeekends;

    #[ORM\OneToMany(targetEntity: Weekend::class, mappedBy: 'chairman', orphanRemoval: false)]
    private $chairmanWeekends;

    #[ORM\OneToMany(targetEntity: Weekend::class, mappedBy: 'reader', orphanRemoval: false)]
    private $readerWeekends;

    #[ORM\Column(type: 'boolean', nullable: true)]
    private bool $disabled = false;

    #[ORM\OneToOne(mappedBy: 'publisher', cascade: ['persist', 'remove'])]
    private ?Wishlist $wishlist;

    #[ORM\OneToMany(mappedBy: 'speaker', targetEntity: SpeechSurvey::class, orphanRemoval: true)]
    private Collection $speechSurveys;

    public function __construct()
    {
        $this->reports = new ArrayCollection();
        $this->speakerWeekends = new ArrayCollection();
        $this->chairmanWeekends = new ArrayCollection();
        $this->readerWeekends = new ArrayCollection();
        $this->speechSurveys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getServiceGroup(): ?ServiceGroup
    {
        return $this->serviceGroup;
    }

    public function setServiceGroup(?ServiceGroup $serviceGroup): self
    {
        $this->serviceGroup = $serviceGroup;

        return $this;
    }

    public function getFullName(): string
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    public function __toString()
    {
        return $this->getFullName();
    }

    public function isIsElder(): ?bool
    {
        return $this->isElder;
    }

    public function setIsElder(bool $isElder): self
    {
        $this->isElder = $isElder;

        return $this;
    }

    public function isIsServant(): ?bool
    {
        return $this->isServant;
    }

    public function setIsServant(bool $isServant): self
    {
        $this->isServant = $isServant;

        return $this;
    }

    public function isIsRegularPioneer(): ?bool
    {
        return $this->isRegularPioneer;
    }

    public function setIsRegularPioneer(bool $isRegularPioneer): self
    {
        $this->isRegularPioneer = $isRegularPioneer;

        return $this;
    }

    public function getDeletedAt(): ?\DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?\DateTimeImmutable $deletedAt): self
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * @return Collection<int, Report>
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setPublisher($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getPublisher() === $this) {
                $report->setPublisher(null);
            }
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        // unset the owning side of the relation if necessary
        if ($user === null && $this->user !== null) {
            $this->user->setPublisher(null);
        }

        // set the owning side of the relation if necessary
        if ($user !== null && $user->getPublisher() !== $this) {
            $user->setPublisher($this);
        }

        $this->user = $user;

        return $this;
    }

    public function getAssignment(): ?Assignment
    {
        return $this->assignment;
    }

    public function setAssignment(?Assignment $assignment): self
    {
        // unset the owning side of the relation if necessary
        if ($assignment === null && $this->assignment !== null) {
            $this->assignment->setPublisher(null);
        }

        // set the owning side of the relation if necessary
        if ($assignment !== null && $assignment->getPublisher() !== $this) {
            $assignment->setPublisher($this);
        }

        $this->assignment = $assignment;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getSpeakerWeekends()
    {
        return $this->speakerWeekends;
    }

    public function getChairmanWeekends()
    {
        return $this->chairmanWeekends;
    }

    public function getReaderWeekends()
    {
        return $this->readerWeekends;
    }

    public function getTotalTimes(): int
    {
        $result = 0;
        foreach ($this->getReports() as $report) {
            $result += $report->getTimes();
        }
        return $result;
    }

    public function getTotalPublications(): int
    {
        $result = 0;
        foreach ($this->getReports() as $report) {
            $result += $report->getPublications();
        }
        return $result;
    }

    public function getTotalOtherTimes(): int
    {
        $result = 0;
        foreach ($this->getReports() as $report) {
            $result += $report->getOtherTimes();
        }
        return $result;
    }

    public function getTotalVideos(): ?int
    {
        $result = 0;
        foreach ($this->getReports() as $report) {
            $result += $report->getVideos();
        }
        return $result;
    }

    public function getTotalVisits(): ?int
    {
        $result = 0;
        foreach ($this->getReports() as $report) {
            $result += $report->getVisits();
        }
        return $result;
    }

    public function getTotalCourses(): ?int
    {
        $result = 0;
        foreach ($this->getReports() as $report) {
            $result += $report->getCourses();
        }
        return $result;
    }

    public function getAvgTimes()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->getReports() as $report) {
            if ($report->getReportCompilation()->isClosed()) {
                $sum += $report->getTimes();
                $count++;
            }
        }
        if ($sum === 0) {
            return 0;
        }

        return $sum / $count;

    }

    public function getAvgPublications()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->getReports() as $report) {
           if ($report->getReportCompilation()->isClosed()) {
               $sum += $report->getPublications();
               $count++;
           }
        }
        if ($sum === 0) {
            return 0;
        }

        return $sum / $count;
    }

    public function getAvgVisits()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->getReports() as $report) {
            if ($report->getReportCompilation()->isClosed()) {
                $sum += $report->getVisits();
                $count++;
            }
        }
        if ($sum === 0) {
            return 0;
        }

        return $sum / $count;
    }

    public function getAvgCourses()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->getReports() as $report) {
            if ($report->getReportCompilation()->isClosed()) {
                $sum += $report->getCourses();
                $count++;
            }
        }
        if ($sum === 0) {
            return 0;
        }

        return $sum / $count;
    }

    public function getAvgOtherTimes()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->getReports() as $report) {
            if ($report->getReportCompilation()->isClosed()) {
                $sum += $report->getOtherTimes();
                $count++;
            }
        }
        if ($sum === 0) {
            return 0;
        }

        return $sum / $count;
    }

    public function getAvgVideos()
    {
        $sum = 0;
        $count = 0;
        foreach ($this->getReports() as $report) {
            if ($report->getReportCompilation()->isClosed()) {
                $sum += $report->getVideos();
                $count++;
            }
        }
        if ($sum === 0) {
            return 0;
        }

        return $sum / $count;
    }

    public function getInitials()
    {
        return mb_substr($this->firstName, 0, 1) . substr($this->lastName, 0, 1);
    }

    public function isDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function addSpeakerWeekend(Weekend $speakerWeekend): static
    {
        if (!$this->speakerWeekends->contains($speakerWeekend)) {
            $this->speakerWeekends->add($speakerWeekend);
            $speakerWeekend->setSpeaker($this);
        }

        return $this;
    }

    public function removeSpeakerWeekend(Weekend $speakerWeekend): static
    {
        if ($this->speakerWeekends->removeElement($speakerWeekend)) {
            // set the owning side to null (unless already changed)
            if ($speakerWeekend->getSpeaker() === $this) {
                $speakerWeekend->setSpeaker(null);
            }
        }

        return $this;
    }

    public function addChairmanWeekend(Weekend $chairmanWeekend): static
    {
        if (!$this->chairmanWeekends->contains($chairmanWeekend)) {
            $this->chairmanWeekends->add($chairmanWeekend);
            $chairmanWeekend->setChairman($this);
        }

        return $this;
    }

    public function removeChairmanWeekend(Weekend $chairmanWeekend): static
    {
        if ($this->chairmanWeekends->removeElement($chairmanWeekend)) {
            // set the owning side to null (unless already changed)
            if ($chairmanWeekend->getChairman() === $this) {
                $chairmanWeekend->setChairman(null);
            }
        }

        return $this;
    }

    public function addReaderWeekend(Weekend $readerWeekend): static
    {
        if (!$this->readerWeekends->contains($readerWeekend)) {
            $this->readerWeekends->add($readerWeekend);
            $readerWeekend->setReader($this);
        }

        return $this;
    }

    public function removeReaderWeekend(Weekend $readerWeekend): static
    {
        if ($this->readerWeekends->removeElement($readerWeekend)) {
            // set the owning side to null (unless already changed)
            if ($readerWeekend->getReader() === $this) {
                $readerWeekend->setReader(null);
            }
        }

        return $this;
    }

    public function getWishlist(): ?Wishlist
    {
        return $this->wishlist;
    }

    public function setWishlist(?Wishlist $wishlist): static
    {
        $this->wishlist = $wishlist;

        return $this;
    }

    /**
     * @return Collection<int, SpeechSurvey>
     */
    public function getSpeechSurveys(): Collection
    {
        return $this->speechSurveys;
    }

    public function addSpeechSurvey(SpeechSurvey $speechSurvey): static
    {
        if (!$this->speechSurveys->contains($speechSurvey)) {
            $this->speechSurveys->add($speechSurvey);
            $speechSurvey->setSpeaker($this);
        }

        return $this;
    }

    public function removeSpeechSurvey(SpeechSurvey $speechSurvey): static
    {
        if ($this->speechSurveys->removeElement($speechSurvey)) {
            // set the owning side to null (unless already changed)
            if ($speechSurvey->getSpeaker() === $this) {
                $speechSurvey->setSpeaker(null);
            }
        }

        return $this;
    }

}
