<?php

namespace App\Form;

use App\Entity\Assignment;
use App\Entity\Publisher;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekendAssignmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('watchtowerReader', null, [
                'label' => 'Lecteur Tour de garde'
            ])
            ->add('weekendChairman', null, [
                'label' => 'Président'
            ])
            ->add('speaker', null, [
                'label' => 'Orateur'
            ])
            ->add('publisher', EntityType::class, [
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('p')
                        ->leftJoin('p.assignment', 'a')
                        ->where('a IS NULL')
                        ->andWhere('p.gender = :gender')
                        ->setParameter('gender', 'M')
                        ->orderBy('p.lastName', 'ASC')
                        ;

                },
                'class' => Publisher::class,
                'attr' => [
                    'class' => 'js-select2'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Assignment::class,
        ]);
    }
}
