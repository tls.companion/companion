<?php

namespace App\Form\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Speech;
use App\Entity\Meeting\Weekend\Wishlist;
use App\Entity\Publisher;
use App\Repository\PublisherRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WishlistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('speeches', EntityType::class, [
                'class' => Speech::class,
                'multiple' => true,
                'choice_label' => 'number',
                'attr' => [
                    'class' => 'js-select2',
                    'data-search' => 'on'
                ],
                'by_reference' => true,
                'label' => 'Thème(s)'

            ])
            ->add('publisher', EntityType::class, [
                'class' => Publisher::class,
                'choice_label' => 'fullName',
                'query_builder' => function(EntityRepository $er) /** @var PublisherRepository $er */ {
                    return $er->createQueryBuilder('p')
                        ->leftJoin('p.assignment', 'a')
                        ->leftJoin('p.wishlist', 'w')
                        ->where('w IS NULL')
                        ->andWhere('a.speaker = 1')
                        ->andWhere('p.deletedAt IS NULL')
                        ;

                },
                'label' => 'Orateur',
                'attr' => [
                    'class' => 'js-select2',
                    'data-search' => 'on'
                ],
                'by_reference' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Wishlist::class
        ]);
    }
}
