<?php

namespace App\Form;

use App\Entity\BoardItem;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class BoardItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
              'label' => 'Titre du document'
            ])
            ->add('document', FileType::class, [
                'label' => 'Fichier (PDF uniquement)',
                'mapped' => false,
                'required' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '2000k',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                    ])
                ],
                'attr' => [
                    'accept' => 'application/pdf'
                ]
            ])
            ->add('category', EntityType::class, [
                'class' => 'App\Entity\BoardCategory',
                'choice_label' => 'title',
                'attr' => [
                    'class' => 'js-select2'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BoardItem::class,
        ]);
    }
}
