<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230401210303 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weekend_category ADD convention_enabled TINYINT(1) DEFAULT NULL, ADD overseer_visit_enabled TINYINT(1) DEFAULT NULL, DROP is_convention, DROP is_overseer_visit');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE weekend_category ADD is_convention TINYINT(1) DEFAULT NULL, ADD is_overseer_visit TINYINT(1) DEFAULT NULL, DROP convention_enabled, DROP overseer_visit_enabled');
    }
}
