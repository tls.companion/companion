<?php

namespace App\Controller\Meeting\Weekend;
use App\Entity\Meeting\Weekend\Program;
use App\Form\Meeting\Weekend\ProgramType;
use App\Repository\Meeting\Weekend\ProgramRepository;
use App\Service\Meeting\Weekend\ProgramService;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Sodium\add;

#[Route(path: '/meeting/weekend/programs')]
class ProgramController extends AbstractController
{

    private ProgramService $service;

    public function __construct(ProgramService $service)
    {
        $this->service = $service;
    }


    #[Route(path: '/create/{month}/{year}', name: 'app_meeting_weekend_program_create')]
    public function create(Request $request, int $month, int $year)
    {
        $program = $this->service->prepareCreate($month, $year);
        $form = $this->createForm(ProgramType::class, $program);
        $form->handleRequest($request);

        if($form->isSubmitted() && !$program->isIsDraft() && $form->isValid()) {
            $this->service->createOrUpdate($program, $this->getUser());
            $this->addFlash('success','Programme enregistré');
            return $this->redirectToRoute('app_meeting_weekend_program_show', ['id' => $program->getId()]);
        }
        if ($form->isSubmitted() && $program->isIsDraft()) {
            $this->service->createOrUpdate($program, $this->getUser());
            $this->addFlash('success','Programme enregistré en tant que brouillon');
            return $this->redirectToRoute('app_meeting_weekend_program_show', ['id' => $program->getId()]);
        }
        return $this->render('meeting/weekend/program/create.html.twig', [
            'program' => $program,
            'form' => $form->createView(),
            'path' => $request->getUri()
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_meeting_weekend_program_edit')]
    public function edit(Program $program, Request $request)
    {
        $form = $this->createForm(ProgramType::class, $program);
        $form->handleRequest($request);

        if($form->isSubmitted() && !$program->isIsDraft() && $form->isValid()) {
            $this->service->createOrUpdate($program);
            $this->addFlash('success','Programme modifié');
            return $this->redirectToRoute('app_meeting_weekend_program_show', ['id' => $program->getId()]);
        }
        if ($form->isSubmitted() && $program->isIsDraft()) {
            $this->service->createOrUpdate($program);
            $this->addFlash('success','Programme enregistré en tant que brouillon');
            return $this->redirectToRoute('app_meeting_weekend_program_show', ['id' => $program->getId()]);
        }
        return $this->render('meeting/weekend/program/create.html.twig', [
            'program' => $program,
            'form' => $form->createView(),
            'path' => $request->getUri()
        ]);
    }

    #[Route(path: '/list/{year}', name: 'app_meeting_weekend_program_list')]
    public function list(ProgramRepository $repository, int $year): Response
    {
        return $this->render('meeting/weekend/program/list.html.twig', [
            'programs' => $repository->findAllByYear($year)
        ]);

    }

    #[Route(path: '/show/{id}', name: 'app_meeting_weekend_program_show')]
    public function show(Program $program)
    {
        return $this->render('meeting/weekend/program/show.html.twig', [
            'program' => $program
        ]);
    }

    #[Route(path: '/delete/{id}', name: 'app_meeting_weekend_program_delete')]
    public function delete(Program $program, ProgramRepository $repository)
    {
        $year = $program->getStartDate()->format('Y');
        $repository->remove($program, true);
        $this->addFlash('success', 'Programme supprimé');
        return $this->redirectToRoute('app_meeting_weekend_program_list', ['year' => $year]);
    }

    #[Route(path: '/export/{id}', name: 'app_meeting_weekend_program_export')]
    public function exportPdf(Program $program, Pdf $pdf)
    {
        $html = $this->renderView('pdf/program_weekend.html.twig', array(
            'program'  => $program
        ));

        return new PdfResponse(
            $pdf->getOutputFromHtml($html),
            $program->getStartDate()->format('F Y') . '.pdf'
        );
    }

}
