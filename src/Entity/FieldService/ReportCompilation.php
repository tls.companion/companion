<?php

namespace App\Entity\FieldService;

use App\Repository\FieldService\ReportCompilationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: ReportCompilationRepository::class)]
class ReportCompilation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'boolean')]
    private $closed = false;

    #[ORM\OneToMany(targetEntity: Report::class, mappedBy: 'reportCompilation', cascade: ['persist'])]
    private $reports;

    #[ORM\ManyToOne(targetEntity: ServiceGroup::class, inversedBy: 'reportCompilations')]
    private $serviceGroup;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'date')]
    private $date;

    /**
     * @var int
     */
    #[Groups(['group_charts'])]
    private $timesAvg;


    public function __construct()
    {
        $this->reports = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isClosed(): ?bool
    {
        return $this->closed;
    }

    public function setClosed(bool $closed): self
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * @return Collection<int, Report>
     */
    public function getReports(): Collection
    {
        return $this->reports;
    }

    public function addReport(Report $report): self
    {
        if (!$this->reports->contains($report)) {
            $this->reports[] = $report;
            $report->setReportCompilation($this);
        }

        return $this;
    }

    public function removeReport(Report $report): self
    {
        if ($this->reports->removeElement($report)) {
            // set the owning side to null (unless already changed)
            if ($report->getReportCompilation() === $this) {
                $report->setReportCompilation(null);
            }
        }

        return $this;
    }

    public function getServiceGroup(): ?ServiceGroup
    {
        return $this->serviceGroup;
    }

    public function setServiceGroup(?ServiceGroup $serviceGroup): self
    {
        $this->serviceGroup = $serviceGroup;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTimesAvg()
    {
        if ($this->getReports()->count() === 0) {
            return 0;
        }
        $nbReports = $this->getReports()->count();
        $sum = 0;
        foreach ($this->getReports() as $report) {
            $sum += $report->getTimes();
        }

        $this->timesAvg = floor(($sum / $nbReports) / 60);
        return $this->timesAvg;
    }

}
