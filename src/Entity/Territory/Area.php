<?php

namespace App\Entity\Territory;

use App\Repository\Territory\AreaRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AreaRepository::class)]
class Area
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 128)]
    private $title;

    #[ORM\Column(type: 'text', nullable: true)]
    private $cloudLink;

    #[ORM\OneToMany(targetEntity: Territory::class, mappedBy: 'area')]
    private $territories;

    private $territoriesWorked;

    private $percentTerritoriesWorked;

    public function __construct()
    {
        $this->territories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCloudLink(): ?string
    {
        return $this->cloudLink;
    }

    public function setCloudLink(?string $cloudLink): self
    {
        $this->cloudLink = $cloudLink;

        return $this;
    }

    /**
     * @return Collection<int, Territory>
     */
    public function getTerritories(): Collection
    {
        return $this->territories;
    }

    public function addTerritory(Territory $territory): self
    {
        if (!$this->territories->contains($territory)) {
            $this->territories[] = $territory;
            $territory->setArea($this);
        }

        return $this;
    }

    public function removeTerritory(Territory $territory): self
    {
        if ($this->territories->removeElement($territory)) {
            // set the owning side to null (unless already changed)
            if ($territory->getArea() === $this) {
                $territory->setArea(null);
            }
        }

        return $this;
    }

    public function resetTerritories()
    {
        foreach ($this->territories as $territory) {
            $this->removeTerritory($territory);
        }
    }


    public function getTerritoriesWorked()
    {
        return $this->territoriesWorked;
    }


    public function setTerritoriesWorked($territoriesWorked): void
    {

        $this->territoriesWorked = $territoriesWorked;
    }

    /**
     * @return mixed
     */
    public function getPercentTerritoriesWorked()
    {
        if ($this->territoriesWorked){
           // dd('test');
           $this->percentTerritoriesWorked = (count($this->territoriesWorked) / count($this->territories)) * 100;
        }
        return $this->percentTerritoriesWorked;
    }


}
