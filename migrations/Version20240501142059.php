<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240501142059 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE board_category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, color VARCHAR(32) DEFAULT NULL, position INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE board_item (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, category_id INT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, document_file_name VARCHAR(255) NOT NULL, uploaded_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_C58D7C3DB03A8386 (created_by_id), INDEX IDX_C58D7C3D12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE board_item ADD CONSTRAINT FK_C58D7C3DB03A8386 FOREIGN KEY (created_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE board_item ADD CONSTRAINT FK_C58D7C3D12469DE2 FOREIGN KEY (category_id) REFERENCES board_category (id)');
        $this->addSql('ALTER TABLE speech ADD is_priority TINYINT(1) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE board_item DROP FOREIGN KEY FK_C58D7C3DB03A8386');
        $this->addSql('ALTER TABLE board_item DROP FOREIGN KEY FK_C58D7C3D12469DE2');
        $this->addSql('DROP TABLE board_category');
        $this->addSql('DROP TABLE board_item');
        $this->addSql('ALTER TABLE speech DROP is_priority');
    }
}
