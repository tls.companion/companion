<?php

namespace App\Entity\FieldService;

use App\Entity\Publisher;
use App\Repository\FieldService\ServiceGroupRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: ServiceGroupRepository::class)]
class ServiceGroup
{
    #[Groups(['group_charts'])]
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Assert\NotBlank]
    #[ORM\Column(type: 'string', length: 128)]
    private $name;

    #[ORM\OneToMany(targetEntity: Publisher::class, mappedBy: 'serviceGroup', cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: true, onDelete: 'SET NULL')]
    private $publishers;

    #[ORM\OneToOne(targetEntity: Publisher::class, cascade: ['persist'])]
    #[ORM\JoinColumn(nullable: false)]
    private $overseer;

    #[ORM\OneToOne(targetEntity: Publisher::class, cascade: ['persist'])]
    private $assistant;

    #[Groups(['group_charts'])]
    #[ORM\OneToMany(targetEntity: ReportCompilation::class, mappedBy: 'serviceGroup', cascade: ['persist'])]
    private $reportCompilations;

    public function __construct()
    {
        $this->publishers = new ArrayCollection();
        $this->reportCompilations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Publisher>
     */
    public function getPublishers(): Collection
    {
        return $this->publishers;
    }

    public function addPublisher(Publisher $publisher): self
    {
        if (!$this->publishers->contains($publisher)) {
            $this->publishers[] = $publisher;
            $publisher->setServiceGroup($this);
        }

        return $this;
    }

    public function removePublisher(Publisher $publisher): self
    {
        if ($this->publishers->removeElement($publisher)) {
            // set the owning side to null (unless already changed)
            if ($publisher->getServiceGroup() === $this) {
                $publisher->setServiceGroup(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getOverseer(): ?Publisher
    {
        return $this->overseer;
    }

    public function setOverseer(Publisher $overseer): self
    {
        $this->overseer = $overseer;
        $this->addPublisher($overseer);

        return $this;
    }

    public function getAssistant(): ?Publisher
    {
        return $this->assistant;
    }

    public function setAssistant(?Publisher $assistant): self
    {
        $this->assistant = $assistant;
        $this->addPublisher($assistant);

        return $this;
    }

    #[ORM\PreRemove]
    public function onRemoveGroup(){
      foreach ($this->getPublishers() as $publisher) {
          $this->removePublisher($publisher);
      }
    }

    /**
     * @return Collection<int, ReportCompilation>
     */
    public function getReportCompilations(): Collection
    {
        return $this->reportCompilations;
    }

    public function addReportCompilation(ReportCompilation $reportCompilation): self
    {
        if (!$this->reportCompilations->contains($reportCompilation)) {
            $this->reportCompilations[] = $reportCompilation;
            $reportCompilation->setServiceGroup($this);
        }

        return $this;
    }

    public function removeReportCompilation(ReportCompilation $reportCompilation): self
    {
        if ($this->reportCompilations->removeElement($reportCompilation)) {
            // set the owning side to null (unless already changed)
            if ($reportCompilation->getServiceGroup() === $this) {
                $reportCompilation->setServiceGroup(null);
            }
        }

        return $this;
    }
}
