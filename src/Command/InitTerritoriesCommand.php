<?php

namespace App\Command;

use App\Entity\Meeting\Weekend\Speech;
use App\Entity\Territory\Territory;
use App\Repository\Meeting\Weekend\SpeechCategoryRepository;
use App\Repository\Meeting\Weekend\SpeechRepository;
use App\Repository\Territory\AreaRepository;
use App\Repository\Territory\TerritoryRepository;
use League\Csv\Reader;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InitTerritoriesCommand extends Command
{
    protected static $defaultName = 'app:init-territories';
    protected static $defaultDescription = "Initialisation des territoires";

    private LoggerInterface $logger;
    private TerritoryRepository  $repository;

    private AreaRepository $areaRepository;

    public function __construct(LoggerInterface $logger, TerritoryRepository $repository, AreaRepository $areaRepository)
    {
        $this->logger = $logger;
        $this->repository = $repository;
        $this->areaRepository = $areaRepository;

        // you *must* call the parent constructor
        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

       $areas = [
           [
               'name' => "Patte d'Oie",
               'nbTerr' => 69
           ],
           [
               'name' => "Cépière",
               'nbTerr' => 35
           ],
           [
               'name' => "Cartoucherie",
               'nbTerr' => 57
           ],
           [
               'name' => "Casselardit",
               'nbTerr' => 58
           ],
           [
               'name' => "Amidonniers",
               'nbTerr' => 35
           ],
           [
               'name' => "Saint Sernin",
               'nbTerr' => 130
           ],
           [
               'name' => "Commerce",
               'nbTerr' => 10
           ],
           [
               'name' => "Sept Deniers",
               'nbTerr' => 63
           ],
       ];

       $count = 0;
        foreach ($areas as $area) {
            $areaFromRepo = $this->areaRepository->findOneBy(['title' => $area['name']]);
            if ($areaFromRepo) {
                for ($i = 1; $i <= $area['nbTerr']; $i++) {
                    $count++;
                    $newTerr = new Territory();
                    $newTerr
                        ->setTitle($i)
                        ->setArea($areaFromRepo)
                        ;
                   $this->repository->add($newTerr, true);
                }

            }
        }


        $this->logger->info('Les territoires ont été ajoutés via la commande app:init-territories');
        $io->success($count . " territoires ont été ajoutés ont été ajoutés");

        return Command::SUCCESS;
    }
}
