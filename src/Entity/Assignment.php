<?php

namespace App\Entity;

use App\Repository\AssignmentRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AssignmentRepository::class)]
class Assignment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'boolean')]
    private $watchtowerReader;

    #[ORM\Column(type: 'boolean')]
    private $weekendChairman;

    #[ORM\Column(type: 'boolean')]
    private $speaker;

    #[ORM\OneToOne(targetEntity: Publisher::class, inversedBy: 'assignment', cascade: ['persist'])]
    private $publisher;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function isWatchtowerReader(): ?bool
    {
        return $this->watchtowerReader;
    }

    public function setWatchtowerReader(bool $watchtowerReader): self
    {
        $this->watchtowerReader = $watchtowerReader;

        return $this;
    }

    public function isWeekendChairman(): ?bool
    {
        return $this->weekendChairman;
    }

    public function setWeekendChairman(bool $weekendChairman): self
    {
        $this->weekendChairman = $weekendChairman;

        return $this;
    }

    public function isSpeaker(): ?bool
    {
        return $this->speaker;
    }

    public function setSpeaker(bool $speaker): self
    {
        $this->speaker = $speaker;

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }
}
