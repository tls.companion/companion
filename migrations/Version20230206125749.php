<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230206125749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE service_group ADD overseer_id INT NOT NULL, ADD assistant_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE service_group ADD CONSTRAINT FK_C4B2A9222720AC9C FOREIGN KEY (overseer_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE service_group ADD CONSTRAINT FK_C4B2A922E05387EF FOREIGN KEY (assistant_id) REFERENCES publisher (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C4B2A9222720AC9C ON service_group (overseer_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_C4B2A922E05387EF ON service_group (assistant_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE group_overseer (id INT AUTO_INCREMENT NOT NULL, publisher_id INT NOT NULL, service_group_id INT NOT NULL, is_assistant TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_A05D601D40C86FCE (publisher_id), INDEX IDX_A05D601D722827A (service_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE group_overseer ADD CONSTRAINT FK_A05D601D40C86FCE FOREIGN KEY (publisher_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE group_overseer ADD CONSTRAINT FK_A05D601D722827A FOREIGN KEY (service_group_id) REFERENCES service_group (id)');
        $this->addSql('ALTER TABLE service_group DROP FOREIGN KEY FK_C4B2A9222720AC9C');
        $this->addSql('ALTER TABLE service_group DROP FOREIGN KEY FK_C4B2A922E05387EF');
        $this->addSql('DROP INDEX UNIQ_C4B2A9222720AC9C ON service_group');
        $this->addSql('DROP INDEX UNIQ_C4B2A922E05387EF ON service_group');
        $this->addSql('ALTER TABLE service_group DROP overseer_id, DROP assistant_id');
    }
}
