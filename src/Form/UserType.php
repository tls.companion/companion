<?php

namespace App\Form;

use App\Entity\Publisher;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'required' => true
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Administrateur' => 'ROLE_SUPER_ADMIN',
                    'Support technique' => 'ROLE_ADMIN',
                    'Coordinateur du collège' => 'ROLE_COORDINATOR',
                    'Secrétaire' => 'ROLE_SECRETARY',
                    'Utilisateur' => 'ROLE_USER',
                    'Responsable des programmes des discours publics' => 'ROLE_PUBLIC_TALK_OVERSEER',
                    'Adjoint aux programmes des discours publics' => 'ROLE_PUBLIC_TALK_ASSISTANT',
                    'Responsable réunion VCM' => 'ROLE_CHRISTIAN_LIFE_MINISTRY_OVERSEER',
                    'Adjoint programme réunion VCM' => 'ROLE_CHRISTIAN_LIFE_MINISTRY_ASSISTANT',
                    'Préposé aux territoires' => 'ROLE_TERRITORY'

                ],
                'multiple' => true,
                'required' => false,
                'label' => 'Droit(s)',
                'attr' => [
                    'class' => 'js-select2'
                ]
            ])
            ->add('firstName', null, [
                'label' => 'Prénom',
                'required' => true
            ])
            ->add('lastName', null, [
                'label' => 'Nom de famille',
                'required' => true
            ])
            ->add('publisher', EntityType::class, [
                'required' => false,
                'class' => Publisher::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->where('p.deletedAt IS NULL')
                        ->orderBy('p.lastName', 'ASC')
                        ;
                },
                'attr' => [
                    'class' => 'js-select2',
                    'data-search' => 'on'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'validation_groups' => ['create']
        ]);
    }
}
