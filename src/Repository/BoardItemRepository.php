<?php

namespace App\Repository;

use App\Entity\BoardItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BoardItem>
 *
 * @method BoardItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoardItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoardItem[]    findAll()
 * @method BoardItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoardItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoardItem::class);
    }

    public function add(BoardItem $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BoardItem $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
