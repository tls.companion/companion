<?php

namespace Service\Utils;

use App\Service\Utils\Meeting\Weekend\ProgramUtils;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProgramUtilsTest extends KernelTestCase
{

  private ProgramUtils $utils;

  protected function setUp(): void
  {
    self::bootKernel();
    $container = static::getContainer();
    $this->utils = $container->get(ProgramUtils::class);
  }

  public function testGetWeekendsPeriodShouldRetrieveSunday()
  {
    $month = 9;
    $year = 2023;


    $period = $this->utils->getWeekendsPeriod($month, $year);
    $weekends = iterator_to_array($period);

    $expectedWeekends = [
      new DateTime('2023-09-03'),
      new DateTime('2023-09-10'),
      new DateTime('2023-09-17'),
      new DateTime('2023-09-24'),
    ];

    $this->assertCount(count($expectedWeekends), $weekends);

    foreach ($weekends as $key => $weekend) {
      $this->assertEquals($expectedWeekends[$key]->format('Y-m-d'), $weekend->format('Y-m-d'));
    }
  }

}
