<?php

namespace App\Controller\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Speech;
use App\Form\Meeting\Weekend\SpeechType;
use App\Repository\Meeting\Weekend\SpeechRepository;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/meeting/weekend/speeches')]
class SpeechController extends AbstractController
{

    protected SpeechRepository $repository;

    /**
     * @param SpeechRepository $repository
     */
    public function __construct(SpeechRepository $repository)
    {
        $this->repository = $repository;
    }


    #[Route(name: 'app_meeting_weekend_speech_list')]
    public function list(Request $request, PaginatorInterface $paginator)
    {
        $query = $this->repository->findAllForPaginator();
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            50 /*limit per page*/,
            ['wrap-queries' => true]
        );

        return $this->render('meeting/weekend/speech/list.html.twig',[
            'speeches' => $pagination
        ]);
    }

    #[Route(path: '/last/{id}', name: 'app_meeting_weekend_speech_last_time')]
    public function showLastTime(int $id)
    {
        return new JsonResponse($this->repository->findLastTime($id));
    }

    #[Route(path: '/autocomplete', name: 'app_meeting_weekend_speech_autocomplete')]
    public function autocomplete(SpeechRepository $repository, PaginatorInterface $paginator, Request $request)
    {
        $term = $request->query->get('term');
        $query = $repository->autocomplete($term);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            50 /*limit per page*/,
            ['wrap-queries' => true]
        );

        return $this->render('meeting/weekend/speech/_ajax_list.html.twig',[
            'speeches' => $pagination
        ]);
    }

    #[Route(path: '/create', name: 'app_meeting_weekend_speech_create')]
    public function create(Request $request)
    {
        $speech = new Speech();
        $form = $this->createForm(SpeechType::class, $speech);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->repository->add($speech, true);
            $this->addFlash('success', 'Theme de discours ajouté à la S-99');
            return $this->redirectToRoute('app_meeting_weekend_speech_list');
        }
        return $this->render('meeting/weekend/speech/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_meeting_weekend_speech_edit')]
    public function edit(Speech $speech, Request $request)
    {
        $form = $this->createForm(SpeechType::class, $speech);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->repository->add($speech, true);
            $this->addFlash('success', 'Theme de discours modifié');
            return $this->redirectToRoute('app_meeting_weekend_speech_list');
        }
        return $this->render('meeting/weekend/speech/edit.html.twig', [
            'form' => $form->createView(),
            'speech' => $speech
        ]);
    }

    #[Route(path: '/export/notgiven', name: 'app_meeting_weekend_speech_notgiven')]
    public function exportNotGiven(SpeechRepository $repository, Pdf $pdf)
    {
        $html = $this->renderView('pdf/speeches_not_given.html.twig', [
            'speeches'  => $repository->findNotGiven()
        ]);

        return new PdfResponse(
            $pdf->getOutputFromHtml($html),
            'discours_disponible.pdf'
        );

    }

    #[Route(path: '/show/{number}', name: 'app_meeting_weekend_speech_show')]
    public function show(int $number, SpeechRepository $repository): Response
    {
        return  $this->render('meeting/weekend/speech/show.html.twig', [
            'speech'  => $repository->findOneBy(['number' => $number])
        ]);

    }

  #[Route(path: '/toggle/ready/{speech}', name: 'app_meeting_weekend_speech_toggle_ready')]
  public function toggleReady(Speech $speech): RedirectResponse
  {
    if ($speech->isPresentationReady()) {
      $this->addFlash('success', "Discours n°" . $speech->getNumber() . " retiré de la liste des discours à présenter prochainement");
      $speech->setPresentationReady(false);
    } else {
      $this->addFlash('success', "Discours n°" . $speech->getNumber() . " marqué comme disponible");
      $speech->setPresentationReady(true);
    }

    $this->repository->add($speech, true);

    return $this->redirectToRoute('app_meeting_weekend_speech_list');

  }

  #[Route(path: '/toggle/priority/{speech}', name: 'app_meeting_weekend_speech_toggle_priority')]
  public function togglePriority(Speech $speech): RedirectResponse
  {
    if ($speech->isIsPriority()) {
      $this->addFlash('success', "Discours n°" . $speech->getNumber() . " retiré de la liste des discours prioritaire");
      $speech->setIsPriority(false);
    } else {
      $this->addFlash('success', "Discours n°" . $speech->getNumber() . " marqué comme prioritaire");
      $speech->setIsPriority(true);
    }

    $this->repository->add($speech, true);

    return $this->redirectToRoute('app_meeting_weekend_speech_list');

  }

  #[Route(path: '/delete/{speech}', name: 'app_meeting_weekend_speech_delete')]
  public function delete(Speech $speech)
  {
    $speech->setDeletedAt(new \DateTimeImmutable("now"));
    $this->repository->add($speech, true);
    $this->addFlash('success', "Discours n°" . $speech->getNumber() . " supprimé");
    return $this->redirectToRoute('app_meeting_weekend_speech_list');
  }

}
