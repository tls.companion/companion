<?php

namespace App\Repository\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Program;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Program>
 *
 * @method Program|null find($id, $lockMode = null, $lockVersion = null)
 * @method Program|null findOneBy(array $criteria, array $orderBy = null)
 * @method Program[]    findAll()
 * @method Program[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProgramRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Program::class);
    }

    public function add(Program $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Program $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllByYear(int $year)
    {
        $nextYear = $year+1;
        $january = new \DateTime("$year-01-01");
        $nextJanuary = new \DateTime("$nextYear-01-01");

        return $this->createQueryBuilder('p')
            ->where('p.startDate BETWEEN :january AND :nextJanuary')
            ->setParameters([
                'january' => $january,
                'nextJanuary' => $nextJanuary
            ])
            ->orderBy('p.startDate', 'ASC')
            ->getQuery()
            ->getResult();
    }
}
