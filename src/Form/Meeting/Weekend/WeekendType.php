<?php

namespace App\Form\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Speech;
use App\Entity\Meeting\Weekend\Weekend;
use App\Entity\Meeting\Weekend\WeekendCategory;
use App\Entity\Publisher;
use App\Repository\Meeting\Weekend\SpeechRepository;
use App\Repository\Meeting\Weekend\WeekendCategoryRepository;
use App\Repository\PublisherRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('date', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('externalSpeaker', TextType::class, [
                'attr' => [
                    'class' => 'external-input d-none',
                    'placeholder' => 'Nom de l\'orateur visiteur',
                ],
                'required' => false
            ])
            ->add('category', EntityType::class, [
                'class' => WeekendCategory::class,
                'choice_name' => 'name',
                'attr' => [
                    'class' => 'js-select2 category-input',
                ],
                'query_builder' => function(EntityRepository $er) /** @var WeekendCategoryRepository $er */ {
                    return $er->findAllForForm();
                }

            ])
            ->add('speaker', EntityType::class, [
                'class' => Publisher::class,
                'choice_name' => 'fullName',
                'placeholder' => "Choix de l'orateur",
                'attr' => [
                    'class' => 'js-select2 speaker-input',
                    'data-search' => 'on',
                    'data-allow-clear' => 'true',
                ],
                'query_builder' => function (EntityRepository $er) { /** @var PublisherRepository $er */
                    return $er->findForWeekendProgramForm('speaker');
                },
                'required' => false,
            ] )
            ->add('reader', EntityType::class, [
                'class' => Publisher::class,
                'placeholder' => 'Choix du lecteur',
                'choice_name' => 'fullName',
                'attr' => [
                    'class' => 'js-select2 reader-input',
                    'data-search' => 'on',
                    'data-allow-clear' => 'true',
                    'data-placeholder' => 'true'
                ],
                'query_builder' => function (EntityRepository $er) { /** @var PublisherRepository $er */
                    return $er->findForWeekendProgramForm('reader');
                },
                'required' => false,

            ])
            ->add('chairman', EntityType::class, [
                'class' => Publisher::class,
                'placeholder' => 'Choix du président',
                'attr' => [
                    'class' => 'js-select2 chairman-input',
                    'data-search' => 'on',
                    'data-allow-clear' => 'true',
                    'data-placeholder' => 'true'
                ],
                'query_builder' => function (EntityRepository $er) { /** @var PublisherRepository $er */
                    return $er->findForWeekendProgramForm('chairman');
                },
                'required' => false,
            ])
            ->add('speech', EntityType::class, [
                'class' => Speech::class,
                'attr' => [
                    'class' => 'js-select2 speech-input',
                    'data-search' => 'on',
                    'data-allow-clear' => 'true',
                    'data-placeholder' => 'true'
                ],
                'query_builder' => function (EntityRepository $er) { /** @var SpeechRepository $er */
                    return $er->findFormForm();
                },
                'placeholder' => 'Choix du discours',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Weekend::class,
        ]);
    }
}
