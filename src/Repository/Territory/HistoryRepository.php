<?php

namespace App\Repository\Territory;

use App\Entity\Territory\History;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<History>
 *
 * @method History|null find($id, $lockMode = null, $lockVersion = null)
 * @method History|null findOneBy(array $criteria, array $orderBy = null)
 * @method History[]    findAll()
 * @method History[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, History::class);
    }

    public function add(History $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(History $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }


    public function findByServiceGroup(int $idGroup)
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.publisher', 'p')
            ->leftJoin('p.serviceGroup', 'sg')
            ->where('sg.id = :idGroup')
            ->andWhere('h.inDate is NULL')
            ->setParameter('idGroup', $idGroup)
            ->orderBy('h.outDate', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByPublisher(int $idPublisher)
    {
        return $this->createQueryBuilder('h')
            ->leftJoin('h.publisher', 'p')
            ->leftJoin('h.territory', 't')
            ->leftJoin('t.area', 'a')
            ->where('p.id = :idPublisher')
            ->andWhere('h.inDate is NULL')
            ->setParameter('idPublisher', $idPublisher)
            ->orderBy('h.outDate', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

  public function findCreatedByUserForTracking(int $userId)
  {
    return $this->createQueryBuilder('h')
      ->leftJoin('h.createdBy', 'u')
      ->leftJoin('h.territory', 't')
      ->leftJoin('t.area', 'a')
      ->where('u.id = :userId')
      ->andWhere('h.inDate is NULL')
      ->setParameter('userId', $userId)
      ->orderBy('h.outDate', 'ASC')
      ->getQuery()
      ->getResult()
      ;
  }
}
