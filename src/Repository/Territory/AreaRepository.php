<?php

namespace App\Repository\Territory;

use App\Entity\Territory\Area;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Area>
 *
 * @method Area|null find($id, $lockMode = null, $lockVersion = null)
 * @method Area|null findOneBy(array $criteria, array $orderBy = null)
 * @method Area[]    findAll()
 * @method Area[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AreaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Area::class);
    }

    public function add(Area $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Area $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAllForList()
    {
        return $this->createQueryBuilder('a')
            ->select('a.title, COUNT(t.id) AS countTerritories, a.cloudLink, a.id')
            ->leftJoin('a.territories', 't')
            ->groupBy('a.title')
            ->addGroupBy('a.cloudLink')
            ->addGroupBy('a.id')
            ->getQuery()
            ->getResult()
            ;
    }

    public function findByIdForShow(int $id)
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->addSelect('t')
            ->addSelect('h')
            ->leftJoin('a.territories', 't')
            ->leftJoin('t.histories', 'h', 'WITH', 'h.outDate = (SELECT MAX(h2.outDate) FROM \App\Entity\Territory\History as h2 WHERE h2.territory = t.id)')
            ->orderBy('t.title + 0', 'ASC')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult()
            ;
    }

  /**
   * @param string $startDate
   * @param string $endDate
   * @return Area[]
   * @throws \Exception
   */
    public function findForExportS13(): array
    {
        return $this->createQueryBuilder('a')
            ->addSelect('t')
            ->addSelect('h')
            ->leftJoin('a.territories', 't')
            ->leftJoin('t.histories', 'h')
            ->orderBy('a.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findForKpi()
    {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.territories', 't')
            ->leftJoin('t.histories', 'h')
            ->orderBy('a.title', 'ASC')
            ->where('h.outDate BETWEEN :september AND :august')
            ->andWhere('h.inDate BETWEEN :september AND :august')
            ->setParameters([
                'september' => new \DateTime('2022-09-01'),
                'august' => new \DateTime('2024-08-31')
            ])
            ->getQuery()
            ->getResult();
    }
}
