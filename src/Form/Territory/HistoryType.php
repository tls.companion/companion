<?php

namespace App\Form\Territory;

use App\Entity\Publisher;
use App\Entity\Territory\History;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HistoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('outDate', DateType::class, [
                'label' => 'Date de sortie',
                'widget' => 'single_text',
                'attr' => [
                    'class' => '',
                 //   'data-date-format' => 'dd/mm/yyyy',
                    'data-date-language' => 'fr'
                ],
              //  'format' => 'dd/MM/yyyy',
                'html5' => true
            ])
            ->add('inDate', DateType::class, [
                'label' => 'Date de remise',
                'widget' => 'single_text',
                'html5' => true,
                'required' => false,
                'attr' => [
                    'class' => '',
                  //  'data-date-format' => 'dd/mm/yyyy',
                    'data-date-language' => 'fr'
                ],
              //  'format' => 'dd/MM/yyyy',
            ])
            ->add('publisher', EntityType::class, [
                'class' => Publisher::class,
                'choice_label' => 'fullName',
                'label' => 'Proclamateur',
                'attr' => [
                    'class' => 'js-select2',
                    'data-search' => 'on',
                    'placeholder' => 'Choix du proclamateur',
                    'data-placeholder' => 'Choix du proclamateur'
                ],
                'query_builder' => function(EntityRepository $er) {
                    return $er
                        ->createQueryBuilder('p')
                        ->where('p.deletedAt is null')
                        ->andWhere('p.disabled = false')
                        ->orderBy('p.lastName', 'ASC')
                        ;
                },
                'placeholder' => 'Choix du proclamateur',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => History::class,
        ]);
    }
}
