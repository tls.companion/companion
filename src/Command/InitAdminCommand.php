<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class InitAdminCommand extends Command
{
    protected static $defaultName = 'app:init-admin';
    protected static $defaultDescription = "Initialisation d'un compte administrateur.";

    private LoggerInterface $logger;
    private ValidatorInterface $validator;
    private UserPasswordHasherInterface $hasher;
    private EntityManagerInterface $em;

    public function __construct(LoggerInterface $logger, ValidatorInterface $validator, UserPasswordHasherInterface $hasher, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->validator = $validator;
        $this->hasher = $hasher;
        $this->em = $em;


        // you *must* call the parent constructor
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('firstname', InputArgument::REQUIRED, 'Prénom')
            ->addArgument('lastName', InputArgument::REQUIRED, 'Nom de famille')
            ->addArgument('email', InputArgument::REQUIRED, 'Email')
        ;
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $firstName = $input->getArgument('firstname');
        $lastName = $input->getArgument('lastName');
        $email = $input->getArgument('email');
        $defaultPassword = $_ENV['DEFAULT_ADMIN_PASSWORD'];


        $io->note(sprintf('Prénom : %s', $firstName));
        $io->note(sprintf('Nom : %s', $lastName));
        $io->note(sprintf('Email : %s', $email));

        $user = new User();
        $passwordValid = preg_match("/^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)(?=.*[#$@!%\-&*?§=}]).{8,255}$/", $defaultPassword);
        if ($passwordValid === 1) {
            $password = $this->hasher->hashPassword($user, $defaultPassword);
        } else {
            $io->error("Le mot de passe n'est pas valide");
            return Command::FAILURE;
        }

        $user
            ->setLastName($lastName)
            ->setFirstName($firstName)
            ->setEmail($email)
            ->setPassword($password)
            ->setRoles(['ROLE_SUPER_ADMIN'])
            ->setIsActive(true);

        $errors = $this->validator->validate($user);

        if ($errors->count() > 0) {
            $io->error((string) $errors);
            return Command::FAILURE;
        }

        $this->em->persist($user);
        $this->em->flush();

        $io->success("L'admin a été crée en base de donnée avec le mot de passe suivant : " . $defaultPassword);

        return Command::SUCCESS;
    }
}
