<?php

namespace App\Entity\FieldService;

use App\Entity\Publisher;
use App\Entity\User;
use App\Repository\FieldService\ReportRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\HasLifecycleCallbacks]
#[ORM\Entity(repositoryClass: ReportRepository::class)]
class Report
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'date')]
    private $date;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $times;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $videos;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $visits;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $courses;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'text', nullable: true)]
    private $comment;

    #[Groups(['group_charts'])]
    #[ORM\Column(type: 'integer', nullable: true)]
    private $publications;

    #[ORM\ManyToOne(targetEntity: Publisher::class, inversedBy: 'reports')]
    #[ORM\JoinColumn(nullable: false)]
    private $publisher;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private $createdBy;

    #[ORM\ManyToOne(targetEntity: ReportCompilation::class, inversedBy: 'reports')]
    private $reportCompilation;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $otherTimes = 0;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getTimes(): ?int
    {
        return $this->times;
    }

    public function setTimes(?int $times): self
    {
        $this->times = $times;

        return $this;
    }

    public function getVideos(): ?int
    {
        return $this->videos;
    }

    public function setVideos(?int $videos): self
    {
        $this->videos = $videos;

        return $this;
    }

    public function getVisits(): ?int
    {
        return $this->visits;
    }

    public function setVisits(?int $visits): self
    {
        $this->visits = $visits;

        return $this;
    }

    public function getCourses(): ?int
    {
        return $this->courses;
    }

    public function setCourses(?int $courses): self
    {
        $this->courses = $courses;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPublications(): ?int
    {
        return $this->publications;
    }

    public function setPublications(?int $publications): self
    {
        $this->publications = $publications;

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): self
    {
        $this->publisher = $publisher;

        return $this;
    }

    public function getCreatedBy(): ?User
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?UserInterface $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getReportCompilation(): ?ReportCompilation
    {
        return $this->reportCompilation;
    }

    public function setReportCompilation(?ReportCompilation $reportCompilation): self
    {
        $this->reportCompilation = $reportCompilation;

        return $this;
    }

    public function getOtherTimes(): ?int
    {
        return $this->otherTimes;
    }

    public function setOtherTimes(?int $otherTimes): self
    {
        $this->otherTimes = $otherTimes;

        return $this;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function setOtherTimeToMinute()
    {
        $this->otherTimes = $this->otherTimes * 60;
    }
}
