<?php

namespace App\Form\FieldService;

use App\Entity\FieldService\ServiceGroup;
use App\Entity\Publisher;
use App\Repository\PublisherRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditServiceGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'required' => true,
                'label' => 'Nom du groupe',
                'attr' => [
                    'placeholder' => 'ex: Cartoucherie'
                ]
            ])
            ->add('overseer', EntityType::class, [
                'class' => Publisher::class,
                'required' => true,
                'attr' => [
                    'class' => 'js-select2'
                ],
                'label' => 'Responsable de groupe',
                'query_builder' => function(EntityRepository $er) { /** @var $er PublisherRepository */
                    return $er->createQueryBuilder('p')
                        ->where('p.isElder = 1')
                        ;
                }
            ])
            ->add('assistant', EntityType::class, [
                'class' => Publisher::class,
                'required' => false,
                'attr' => [
                    'class' => 'js-select2'
                ],
                'label' => 'Adjoint au responsable',
                'query_builder' => function(EntityRepository $er) { /** @var $er PublisherRepository */
                    return $er->createQueryBuilder('p')
                        ->where('p.isElder = 1')
                        ->orWhere('p.isServant = 1')
                        ->andWhere('p.deletedAt IS NULL')
                        ;
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ServiceGroup::class
        ]);
    }
}
