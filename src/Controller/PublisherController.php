<?php

namespace App\Controller;

use App\Entity\Publisher;
use App\Form\PublisherType;
use App\Repository\PublisherRepository;
use App\Service\PublisherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/publishers')]
class PublisherController extends AbstractController
{
    private PublisherService $service;

    public function __construct(PublisherService $service)
    {
        $this->service = $service;
    }

    #[Route(name: 'app_publisher_list')]
    public function list(PublisherRepository $repository): Response
    {
        return $this->render('publisher/list.html.twig', [
            'publishers' => $repository->findBy([
                'deletedAt' => null,
                'disabled' => false
            ],
                ['lastName' => 'ASC'])
        ]);
    }

    #[Route(path: '/create', name: 'app_publisher_create')]
    public function create(Request $request): Response
    {
        $publisher = new Publisher();
        $form = $this->createForm(PublisherType::class, $publisher);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->service->create($publisher);
            $this->addFlash('success', 'La fiche de ' . $publisher->getFullName() .  ' a été créée.');
            if ($request->query->get('createNext')) {
                return $this->redirectToRoute('app_publisher_create');
            }
            return $this->redirectToRoute('app_publisher_list');
        }

        return $this->render('publisher/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route(path: '/show/{id}', name: 'app_publisher_show')]
    public function show(Publisher $publisher, Request $request, SerializerInterface $serializer): Response
    {
        $form = $this->createForm(PublisherType::class, $publisher);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->service->create($publisher);
            $this->addFlash('success', 'Mis à jour');
            return $this->redirectToRoute('app_publisher_show', ['id' => $publisher->getId()]);
        }
        return $this->render('publisher/show.html.twig', [
            'publisher' => $publisher,
            'form' => $form->createView(),
            'dataCharts' => $serializer->serialize($publisher->getReports(), 'json', [
                'groups' => ['group_charts']
            ]),
            'reports' => $publisher->getReports(),
            'histories' => $this->service->getTerritoriesHistoriesByPublisher($publisher),
            'weekends' => $this->service->getPublicTalks($publisher)
        ]);
    }

    #[Route(path: '/delete/{id}', name: 'app_publisher_delete', methods: ['DELETE'])]
    public function delete(Publisher $publisher): Response
    {
        $this->service->delete($publisher);
        return $this->redirectToRoute('app_publisher_list');
    }

    #[Route(path: '/disable/{id}', name: 'app_publisher_disable')]
    public function disable(Publisher $publisher): Response
    {
        $this->service->disable($publisher);
        $this->addFlash('success', 'Le proclamateur a été désactivé de Companion');
        return $this->redirectToRoute('app_publisher_list');
    }

    #[Route(path: '/search', name: 'app_publisher_search')]
    public function search(Request $request)
    {
        $term = $request->query->get('term') ?: null;
        return $this->render('publisher/fragment/modal_publisher_search.html.twig', [
            'publishers' => $this->service->search($term)
        ]);
    }

}
