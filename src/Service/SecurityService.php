<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class SecurityService
{
    private UserRepository $repository;
    private UserPasswordHasherInterface $hasher;

    /**
     * @param UserRepository $repository
     * @param UserPasswordHasherInterface $hasher
     */
    public function __construct(UserRepository $repository, UserPasswordHasherInterface $hasher)
    {
        $this->repository = $repository;
        $this->hasher = $hasher;
    }


    public function resetPassword(User $user, bool $isActivation = false): User
    {
        $password = $this->hasher->hashPassword($user, $user->getPassword());
        $user->setPassword($password);
        if ($isActivation) {
            $user->setIsActive(true);
        }
        $this->repository->add($user, true);
        return $user;
    }

}
