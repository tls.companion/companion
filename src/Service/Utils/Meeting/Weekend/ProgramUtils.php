<?php

namespace App\Service\Utils\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Program;

class ProgramUtils
{

    public function resetUnusedField(Program $program): Program
    {
        foreach ($program->getWeekends() as $weekend) {
            $category = $weekend->getCategory();
            if (!$category->isReaderEnabled()) {
                $weekend->setReader(null);
            }
            if (!$category->isChairmanEnabled()) {
                $weekend->setChairman(null);
            }
            if (!$category->isSpeechEnabled()) {
                $weekend->setSpeech(null);
            }
            if (!$category->isExternalSpeakerEnabled()) {
                $weekend->setExternalSpeaker('');
            }
            if ($category->isExternalSpeakerEnabled() || !$category->isSpeakerEnabled()) {
                $weekend->setSpeaker(null);
            }
        }

        return $program;
    }

    public function getWeekendsPeriod(int $month, int $year): \DatePeriod
    {
        $nextMonth = $month + 1;
        $nextYear = $year;
        if ($month === 12) {
            $nextYear = $year + 1;
            $nextMonth = 01;
        }

        return new \DatePeriod(
            new \DateTime("first sunday of $year-$month"),
            \DateInterval::createFromDateString('next sunday'),
            new \DateTime("first day of $nextYear-$nextMonth")
        );
    }

}
