<?php

namespace App\Entity\Meeting\Weekend;

use App\Repository\Meeting\Weekend\SpeechRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: SpeechRepository::class)]
class Speech
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    private ?string $title;

    #[ORM\Column(type: 'integer', nullable: true)]
    private ?int $number;

    #[ORM\ManyToOne(targetEntity: SpeechCategory::class, inversedBy: 'speeches')]
    private ?SpeechCategory $category;

    #[ORM\OneToMany(mappedBy: 'speech', targetEntity: Weekend::class)]
    private Collection $weekends;

    #[ORM\ManyToMany(targetEntity: Wishlist::class, mappedBy: 'speeches')]
    private Collection $wishlists;

    #[ORM\ManyToMany(targetEntity: SpeechSurvey::class, mappedBy: 'speeches')]
    private Collection $speechSurveys;

    #[ORM\Column(nullable: true)]
    private ?bool $isPriority = null;

    #[ORM\Column(nullable: true)]
    private ?bool $presentationReady = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deletedAt = null;

    public function __construct()
    {
        $this->weekends = new ArrayCollection();
        $this->wishlists = new ArrayCollection();
        $this->speechSurveys = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getNumber(): ?int
    {
        return $this->number;
    }

    public function setNumber(?int $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getCategory(): ?SpeechCategory
    {
        return $this->category;
    }

    public function setCategory(?SpeechCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, Weekend>
     */
    public function getWeekends(): Collection
    {
        return $this->weekends;
    }

    public function addWeekend(Weekend $weekend): self
    {
        if (!$this->weekends->contains($weekend)) {
            $this->weekends[] = $weekend;
            $weekend->setSpeech($this);
        }

        return $this;
    }

    public function removeWeekend(Weekend $weekend): self
    {
        if ($this->weekends->removeElement($weekend)) {
            // set the owning side to null (unless already changed)
            if ($weekend->getSpeech() === $this) {
                $weekend->setSpeech(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        $number = $this->number ? $this->number . ' - ' : null;
        $category = $this->category ? "(" . $this->category->getTitle() . ")" : null;
        return "$number $this->title $category";
    }

    /**
     * @return Collection<int, Wishlist>
     */
    public function getWishlists(): Collection
    {
        return $this->wishlists;
    }

    public function addWishlist(Wishlist $wishlist): static
    {
        if (!$this->wishlists->contains($wishlist)) {
            $this->wishlists->add($wishlist);
            $wishlist->addSpeech($this);
        }

        return $this;
    }

    public function removeWishlist(Wishlist $wishlist): static
    {
        if ($this->wishlists->removeElement($wishlist)) {
            $wishlist->removeSpeech($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, SpeechSurvey>
     */
    public function getSpeechSurveys(): Collection
    {
        return $this->speechSurveys;
    }

    public function addSpeechSurvey(SpeechSurvey $speechSurvey): static
    {
        if (!$this->speechSurveys->contains($speechSurvey)) {
            $this->speechSurveys->add($speechSurvey);
            $speechSurvey->addSpeech($this);
        }

        return $this;
    }

    public function removeSpeechSurvey(SpeechSurvey $speechSurvey): static
    {
        if ($this->speechSurveys->removeElement($speechSurvey)) {
            $speechSurvey->removeSpeech($this);
        }

        return $this;
    }

    public function isIsPriority(): ?bool
    {
        return $this->isPriority;
    }

    public function setIsPriority(?bool $isPriority): static
    {
        $this->isPriority = $isPriority;

        return $this;
    }

  public function forSurvey(): string
  {
    $number = $this->number ? $this->number . ' - ' : null;
   // $category = $this->category ? "(" . $this->category->getTitle() . ")" : null;
    $priority = $this->isPriority ? "[prioritaire]" : null;
    return "$priority $number $this->title";
  }

  public function isPresentationReady(): ?bool
  {
      return $this->presentationReady;
  }

  public function setPresentationReady(?bool $presentationReady): static
  {
      $this->presentationReady = $presentationReady;

      return $this;
  }

  public function getDeletedAt(): ?\DateTimeImmutable
  {
      return $this->deletedAt;
  }

  public function setDeletedAt(?\DateTimeImmutable $deletedAt): static
  {
      $this->deletedAt = $deletedAt;

      return $this;
  }
}
