<?php

namespace App\Validator\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Weekend;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class WeekendValidValidator extends ConstraintValidator
{

    /**
     * @param Weekend $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof WeekendValid) {
            throw new UnexpectedValueException($constraint, WeekendValid::class);
        }

        if (null == $value) {
            return;
        }

        $category = $value->getCategory();

        if (null === $value->getDate()) {
            $this->context->buildViolation($constraint->dateBlank)
                ->atPath('date')
                ->addViolation();
        }

        if ($category->isSpeakerEnabled() && !$value->getSpeaker()) {
            $this->context->buildViolation($constraint->speakerMsg)
                ->atPath('speaker')
                ->addViolation();
        }

        if ($category->isExternalSpeakerEnabled() && !$value->getExternalSpeaker()) {
            $this->context->buildViolation($constraint->externalNameMsg)
                ->atPath('externalSpeaker')
                ->addViolation();
        }

        if ($category->isSpeechEnabled() && !$value->getSpeech()) {
            $this->context->buildViolation($constraint->notBlank)
                ->atPath('speech')
                ->addViolation();
        }

        if ($category->isChairmanEnabled() && !$value->getChairman()) {
            $this->context->buildViolation($constraint->notBlank)
                ->atPath('chairman')
                ->addViolation();
        }

        if ($category->isReaderEnabled() && !$value->getReader()) {
            $this->context->buildViolation($constraint->notBlank)
                ->atPath('reader')
                ->addViolation();
        }
    }


    }
