<?php

namespace App\Service;

use App\Entity\FieldService\Report;
use App\Entity\Meeting\Weekend\Weekend;
use App\Entity\Publisher;
use App\Repository\FieldService\ReportCompilationRepository;
use App\Repository\FieldService\ReportRepository;
use App\Repository\Meeting\Weekend\WeekendRepository;
use App\Repository\PublisherRepository;
use App\Repository\Territory\HistoryRepository;
use Ramsey\Uuid\Uuid;

class PublisherService
{
    private PublisherRepository $repository;
    private ReportCompilationRepository $compilationRepository;
    private ReportRepository $reportRepository;

    private HistoryRepository $historyRepository;

    private WeekendRepository $weekendRepository;

    public function __construct(PublisherRepository $repository,
                                ReportCompilationRepository $compilationRepository,
                                ReportRepository $reportRepository,
                                HistoryRepository $historyRepository,
                                WeekendRepository $weekendRepository

    )
    {
        $this->repository = $repository;
        $this->compilationRepository = $compilationRepository;
        $this->reportRepository = $reportRepository;
        $this->historyRepository = $historyRepository;
        $this->weekendRepository = $weekendRepository;
    }

    public function create(Publisher $publisher): Publisher
    {
        $this->repository->add($publisher, true);
        if ($publisher->getServiceGroup()) {
            foreach ($publisher->getServiceGroup()->getReportCompilations() as $compilation) {
                $report = $this->reportRepository->findOrCreate($compilation->getDate(), $publisher);
                $report->setPublisher($publisher);
                $compilation->addReport($report);
                $this->compilationRepository->add($compilation, true);
            }
        }

        return $publisher;
    }

    public function delete(Publisher $publisher): void
    {
        $token = Uuid::uuid4()->toString();
        $publisher
            ->setFirstName($token)
            ->setLastName($token)
            ->setServiceGroup(null)
            ->setIsServant(false)
            ->setIsElder(false)
            ->setIsRegularPioneer(false)
            ->setDeletedAt(new \DateTimeImmutable())
            ;
        $this->repository->add($publisher, true);
    }

    public function disable(Publisher $publisher)
    {
        $publisher->setDisabled(true);
        $this->repository->add($publisher, true);
    }

    public function getTerritoriesHistoriesByPublisher(Publisher $publisher): array
    {
        return $this->historyRepository->findBy(
            [
            'publisher' => $publisher,
            ],
            [
               'outDate' => 'DESC'
            ]
        );
    }
    public function search(?string $term)
    {
        return $this->repository->autocomplete($term);
    }

    public function getPublicTalks(Publisher $publisher)
    {
        return $this->weekendRepository->findBy(['speaker' => $publisher], ['date' => 'ASC']);
    }

}
