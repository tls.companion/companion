<?php

namespace App\Repository\FieldService;

use App\Entity\FieldService\ReportCompilation;
use App\Entity\FieldService\ServiceGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReportCompilation>
 *
 * @method ReportCompilation|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReportCompilation|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReportCompilation[]    findAll()
 * @method ReportCompilation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReportCompilationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReportCompilation::class);
    }

    public function add(ReportCompilation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ReportCompilation $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function checkMonthToCompile(ServiceGroup $group)
    {
        $currentDate = new \DateTime("first day of now");
        $result = $this->createQueryBuilder('rc')
            ->where('rc.date = :currentDate')
            ->andWhere('rc.serviceGroup = :group')
            ->setParameters([
                'currentDate' => $currentDate->format('Y-m-d'),
                'group' => $group
            ])
            ->getQuery()
            ->getResult()
            ;

        if ($result) {
            return false;
        }

        return $currentDate;
    }

    public function searchEngine($idGroup)
    {
        $qb = $this->createQueryBuilder('rc')
            ->leftJoin('rc.serviceGroup', 'sg')
            ->where('sg.id = :id')
            ->setParameter('id', $idGroup);
        ;

        return $qb
            ->orderBy('rc.date', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findOrCreate(\DateTime $date, ?ServiceGroup $serviceGroup = null)
    {
        if (!$serviceGroup) {
            return null;
        }
        $result = $this->createQueryBuilder('rc')
            ->where('rc.date = :date')
            ->leftJoin('rc.serviceGroup', 'sg')
            ->andWhere('sg.id = :idGroup')
            ->setParameters([
                'date' => $date,
                'idGroup' => $serviceGroup->getId()
            ])
            ->getQuery()
            ->getOneOrNullResult()
        ;

        if (!$result) {
            $compilation = new ReportCompilation();
            $compilation
                ->setServiceGroup($serviceGroup)
                ->setDate($date)
                ->setClosed(true);
            $this->add($compilation, true);
            return $compilation;
        }

        return $result;

    }
}
