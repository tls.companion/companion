<?php

namespace App\Form\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Program;
use App\Entity\Meeting\Weekend\Weekend;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProgramType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('isDraft', CheckboxType::class, [
                'label' => 'Enregistrer en tant que brouillon',
                'required' => false
            ])
            ->add('weekends', CollectionType::class, [
                'entry_type' => WeekendType::class
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Program::class,
        ]);
    }
}
