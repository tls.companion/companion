<?php

namespace App\Repository;

use App\Entity\BoardCategory;
use App\Entity\Publisher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<BoardCategory>
 *
 * @method BoardCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method BoardCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method BoardCategory[]    findAll()
 * @method BoardCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BoardCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BoardCategory::class);
    }

    public function add(BoardCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(BoardCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
