<?php

namespace App\Form\FieldService;

use App\Entity\FieldService\Report;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('times', HiddenType::class, [
                'attr' => ['class' => 'time-input-report']
            ])
            ->add('videos', null, [
                'label' => 'Vidéo(s)'
            ])
            ->add('visits', null, [
                'label' => 'Nouvelle(s) visite(s)'
            ])
            ->add('courses', null, [
                'label' => 'Cours biblique(s)'
            ])
            ->add('comment', TextType::class, [
                'label' => 'Observations',
                'required' => false
            ])
            ->add('publications', null, [
                'label' => 'Publication(s)'
            ])
            ->add('otherTimes', null, [
                'label' => 'Autre (LDC/Bethel)',
                'attr' => [
                    'class' => 'otherTimes',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Report::class,
        ]);
    }
}
