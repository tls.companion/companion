<?php

namespace App\Form;

use App\Entity\Assignment;
use App\Entity\Publisher;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekendAssignmentEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('watchtowerReader', null, [
                'label' => 'Lecteur Tour de garde'
            ])
            ->add('weekendChairman', null, [
                'label' => 'Président'
            ])
            ->add('speaker', null, [
                'label' => 'Orateur'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Assignment::class,
        ]);
    }
}
