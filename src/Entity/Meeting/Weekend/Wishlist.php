<?php

namespace App\Entity\Meeting\Weekend;

use App\Entity\Publisher;
use App\Repository\Meeting\Weekend\WishlistRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WishlistRepository::class)]
class Wishlist
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToMany(targetEntity: Speech::class, inversedBy: 'wishlists')]
    private Collection $speeches;

    #[ORM\OneToOne(inversedBy: 'wishlist', cascade: ['persist', 'remove'])]
    private ?Publisher $publisher = null;

    public function __construct()
    {
        $this->speeches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Speech>
     */
    public function getSpeeches(): Collection
    {
        return $this->speeches;
    }

    public function addSpeech(Speech $speech): static
    {
        if (!$this->speeches->contains($speech)) {
            $this->speeches->add($speech);
        }

        return $this;
    }

    public function removeSpeech(Speech $speech): static
    {
        $this->speeches->removeElement($speech);

        return $this;
    }

    public function getPublisher(): ?Publisher
    {
        return $this->publisher;
    }

    public function setPublisher(?Publisher $publisher): static
    {
        $this->publisher = $publisher;

        return $this;
    }
}
