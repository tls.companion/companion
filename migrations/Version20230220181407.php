<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230220181407 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE program (id INT AUTO_INCREMENT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, is_draft TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speech (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, number INT DEFAULT NULL, INDEX IDX_8AFBE1F712469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speech_category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE weekend (id INT AUTO_INCREMENT NOT NULL, category_id INT NOT NULL, speaker_id INT DEFAULT NULL, reader_id INT DEFAULT NULL, chairman_id INT DEFAULT NULL, program_id INT NOT NULL, speech_id INT DEFAULT NULL, date DATE NOT NULL, external_speaker VARCHAR(255) DEFAULT NULL, INDEX IDX_AED2CDAE12469DE2 (category_id), INDEX IDX_AED2CDAED04A0F27 (speaker_id), INDEX IDX_AED2CDAE1717D737 (reader_id), INDEX IDX_AED2CDAECD0B344F (chairman_id), INDEX IDX_AED2CDAE3EB8070A (program_id), INDEX IDX_AED2CDAEBBC049D6 (speech_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE speech ADD CONSTRAINT FK_8AFBE1F712469DE2 FOREIGN KEY (category_id) REFERENCES speech_category (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAE12469DE2 FOREIGN KEY (category_id) REFERENCES weekend_category (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAED04A0F27 FOREIGN KEY (speaker_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAE1717D737 FOREIGN KEY (reader_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAECD0B344F FOREIGN KEY (chairman_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAE3EB8070A FOREIGN KEY (program_id) REFERENCES program (id)');
        $this->addSql('ALTER TABLE weekend ADD CONSTRAINT FK_AED2CDAEBBC049D6 FOREIGN KEY (speech_id) REFERENCES speech (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE speech DROP FOREIGN KEY FK_8AFBE1F712469DE2');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAE12469DE2');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAED04A0F27');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAE1717D737');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAECD0B344F');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAE3EB8070A');
        $this->addSql('ALTER TABLE weekend DROP FOREIGN KEY FK_AED2CDAEBBC049D6');
        $this->addSql('DROP TABLE program');
        $this->addSql('DROP TABLE speech');
        $this->addSql('DROP TABLE speech_category');
        $this->addSql('DROP TABLE weekend');
    }
}
