<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240501132608 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE speech_survey (id INT AUTO_INCREMENT NOT NULL, speaker_id INT NOT NULL, observations LONGTEXT DEFAULT NULL, token VARCHAR(255) NOT NULL, INDEX IDX_95AD871CD04A0F27 (speaker_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE speech_survey_speech (speech_survey_id INT NOT NULL, speech_id INT NOT NULL, INDEX IDX_D22A9E1FAFD84E68 (speech_survey_id), INDEX IDX_D22A9E1FBBC049D6 (speech_id), PRIMARY KEY(speech_survey_id, speech_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE speech_survey ADD CONSTRAINT FK_95AD871CD04A0F27 FOREIGN KEY (speaker_id) REFERENCES publisher (id)');
        $this->addSql('ALTER TABLE speech_survey_speech ADD CONSTRAINT FK_D22A9E1FAFD84E68 FOREIGN KEY (speech_survey_id) REFERENCES speech_survey (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE speech_survey_speech ADD CONSTRAINT FK_D22A9E1FBBC049D6 FOREIGN KEY (speech_id) REFERENCES speech (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE board_item DROP FOREIGN KEY FK_C58D7C3D12469DE2');
        $this->addSql('ALTER TABLE board_item DROP FOREIGN KEY FK_C58D7C3DB03A8386');
        $this->addSql('DROP TABLE board_category');
        $this->addSql('DROP TABLE board_item');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE board_category (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, color VARCHAR(32) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, position INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('CREATE TABLE board_item (id INT AUTO_INCREMENT NOT NULL, created_by_id INT DEFAULT NULL, category_id INT NOT NULL, title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, slug VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, document_file_name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, uploaded_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_C58D7C3D12469DE2 (category_id), INDEX IDX_C58D7C3DB03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE board_item ADD CONSTRAINT FK_C58D7C3D12469DE2 FOREIGN KEY (category_id) REFERENCES board_category (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE board_item ADD CONSTRAINT FK_C58D7C3DB03A8386 FOREIGN KEY (created_by_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE speech_survey DROP FOREIGN KEY FK_95AD871CD04A0F27');
        $this->addSql('ALTER TABLE speech_survey_speech DROP FOREIGN KEY FK_D22A9E1FAFD84E68');
        $this->addSql('ALTER TABLE speech_survey_speech DROP FOREIGN KEY FK_D22A9E1FBBC049D6');
        $this->addSql('DROP TABLE speech_survey');
        $this->addSql('DROP TABLE speech_survey_speech');
    }
}
