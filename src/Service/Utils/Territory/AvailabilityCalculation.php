<?php

namespace App\Service\Utils\Territory;

use DateTimeImmutable;
use DateTimeInterface;

class AvailabilityCalculation
{

  const EXPIRATION = '3months';

  const WAITING_DELAY = '6months';


  public function isExpired(?DateTimeInterface $date): bool
  {
    $formattedDateWithDelay = $date->format("Y-m-d") . " +" . self::EXPIRATION;

    $expirationDate = new \DateTime($formattedDateWithDelay);
    $now = new \DateTime();
    if ($now > $expirationDate) {
      return true;
    }
    return false;
  }

  public function isNowAvailable(?DateTimeInterface $date): bool
  {
    $formattedDateWithDelay = $date->format("Y-m-d") . " +" . self::WAITING_DELAY;
    $availableDate = new \DateTime($formattedDateWithDelay);
    $now = new \DateTime;
    if ($now > $availableDate) {
      return true;
    }
    return false;
  }

}
