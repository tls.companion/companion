<?php

namespace App\Entity\Meeting\Weekend;

use App\Entity\Publisher;
use App\Repository\Meeting\Weekend\WeekendRepository;
use App\Validator\Meeting\Weekend\WeekendValid;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * @WeekendValid()
 */
#[ORM\Entity(repositoryClass: WeekendRepository::class)]
class Weekend
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'date')]
    private $date;

    #[ORM\ManyToOne(targetEntity: WeekendCategory::class, inversedBy: 'weekends')]
    #[ORM\JoinColumn(nullable: false)]
    private $category;

    #[ORM\ManyToOne(targetEntity: Publisher::class, inversedBy: 'speakerWeekends')]
    private $speaker;

    #[ORM\ManyToOne(targetEntity: Publisher::class, inversedBy: 'readerWeekends')]
    private $reader;

    #[ORM\ManyToOne(targetEntity: Publisher::class, inversedBy: 'chairmanWeekends')]
    private $chairman;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $externalSpeaker;

    #[ORM\ManyToOne(targetEntity: Program::class, inversedBy: 'weekends')]
    #[ORM\JoinColumn(nullable: false)]
    private $program;

    #[ORM\ManyToOne(targetEntity: Speech::class, inversedBy: 'weekends')]
    private $speech;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCategory(): ?WeekendCategory
    {
        return $this->category;
    }

    public function setCategory(?WeekendCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getSpeaker(): ?Publisher
    {
        return $this->speaker;
    }

    public function setSpeaker(?Publisher $speaker): self
    {
        $this->speaker = $speaker;

        return $this;
    }

    public function getReader(): ?Publisher
    {
        return $this->reader;
    }

    public function setReader(?Publisher $reader): self
    {
        $this->reader = $reader;

        return $this;
    }

    public function getChairman(): ?Publisher
    {
        return $this->chairman;
    }

    public function setChairman(?Publisher $chairman): self
    {
        $this->chairman = $chairman;

        return $this;
    }

    public function getExternalSpeaker(): ?string
    {
        return $this->externalSpeaker;
    }

    public function setExternalSpeaker(?string $externalSpeaker): self
    {
        $this->externalSpeaker = $externalSpeaker;

        return $this;
    }

    public function getProgram(): ?Program
    {
        return $this->program;
    }

    public function setProgram(?Program $program): self
    {
        $this->program = $program;

        return $this;
    }

    public function getSpeech(): ?Speech
    {
        return $this->speech;
    }

    public function setSpeech(?Speech $speech): self
    {
        $this->speech = $speech;

        return $this;
    }
}
