<?php

namespace App\Service\Office;

use App\Service\Territory\KPIService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

readonly class TerritoryAllocationRegister
{

  public function __construct(private KPIService $kpiService, private ParameterBagInterface $parameterBag)
  {
  }

  public function write(): Spreadsheet
  {
    $currentDate = new \DateTime();
    $kernelRoot = $this->parameterBag->get('kernel.project_dir');
    $spreadsheet = IOFactory::load("$kernelRoot/templates/export/s13_sample.xlsx");

    $areas = $this->kpiService->s13();
    foreach ($areas as $area) {
      $isLargeSample = false;
      $isMediumSample = false;
      $sampleSheet = $spreadsheet->getSheetByName('sample');
      if ($area->getTitle() === 'Saint Sernin') {
        $isLargeSample = true;
        $sampleSheet = $spreadsheet->getSheetByName('sample_large');
      }
      if ($area->getTitle() === "Patte d'Oie") {
        $isMediumSample = true;
        $sampleSheet = $spreadsheet->getSheetByName('sample_medium');
      }
      $newSheet = clone $sampleSheet;
      $newSheet->setTitle($area->getTitle());

      $spreadsheet->addSheet($newSheet);
      $spreadsheet->setActiveSheetIndex($spreadsheet->getSheetCount() - 1);

      $newSheet->setCellValue('F3', strtoupper($area->getTitle()));

      if ($isMediumSample) {
        $newSheet->setCellValue('B151', $currentDate->format('d.m.y'));
      } elseif($isLargeSample) {
        $newSheet->setCellValue("B276", $currentDate->format('d.m.y'));
      }else {
        $newSheet->setCellValue("B142", $currentDate->format('d.m.y'));
      }


      $startRow = 7;

      foreach ($area->getTerritories() as $territory) {
        $newSheet->setCellValue("A$startRow", $territory->getTitle());
        if ($territory->getLastTerritoryIn()) {
          $newSheet->setCellValue("B$startRow", $territory->getLastTerritoryIn()->getInDate()->format('d/m/Y'));

        }
        foreach ($territory->getHistories() as $key => $history) {
          $startRowPlus1 = $startRow+1;

          $inDateFormatForHuman = $history->getInDate() ? $history->getInDate()->format('d/m/Y') : '';
          $ouDateFormatForHuman = $history->getOutDate() ? $history->getOutDate()->format('d/m/Y') : '';

          $publisherName = $history->getPublisher()->getFullName() !== 'John Doe' ? $history->getPublisher()->getFullName() : '';

          switch ($key) {
            case 0:

              $newSheet->setCellValue("C$startRow", $publisherName);
              $newSheet->setCellValue("C$startRowPlus1", $ouDateFormatForHuman);
              $newSheet->setCellValue("D$startRowPlus1", $inDateFormatForHuman);
              break;

            case 1:
              $newSheet->setCellValue("E$startRow", $publisherName);
              $newSheet->setCellValue("E$startRowPlus1", $ouDateFormatForHuman);
              $newSheet->setCellValue("F$startRowPlus1", $inDateFormatForHuman);
              break;

            case 2:
              $newSheet->setCellValue("G$startRow", $publisherName);
              $newSheet->setCellValue("G$startRowPlus1", $ouDateFormatForHuman);
              $newSheet->setCellValue("H$startRowPlus1", $inDateFormatForHuman);
              break;

            case 3:
              $newSheet->setCellValue("I$startRow", $publisherName);
              $newSheet->setCellValue("I$startRowPlus1", $ouDateFormatForHuman);
              $newSheet->setCellValue("J$startRowPlus1", $inDateFormatForHuman);
              break;
          }
        }
        $startRow = $startRow+2;
      }

    }
    $spreadsheet->removeSheetByIndex(2);
    $spreadsheet->removeSheetByIndex(1);
    $spreadsheet->removeSheetByIndex(0);

    return $spreadsheet;
  }

}
