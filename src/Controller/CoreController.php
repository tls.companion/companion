<?php

namespace App\Controller;

use App\Repository\BoardCategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoreController extends AbstractController
{


    #[Route(path: '/', name: 'app_core_index')]
    public function index(BoardCategoryRepository $boardCategoryRepository): Response
    {
       return $this->redirectToRoute('app_fieldservice_servicegroup_list');

    }

    #[Route(path: '/privacy')]
    public function privacy()
    {
        return $this->render('gdpr/privacy.html.twig');
    }

}
