<?php

namespace App\Command;

use App\Entity\Meeting\Weekend\Speech;
use App\Repository\Meeting\Weekend\SpeechCategoryRepository;
use App\Repository\Meeting\Weekend\SpeechRepository;
use League\Csv\Reader;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class InitSpeechesCommand extends Command
{
    protected static $defaultName = 'app:init-speeches';
    protected static $defaultDescription = "Initialisation des thèmes de discours publics.";

    private LoggerInterface $logger;
    private SpeechRepository $repository;
    private SpeechCategoryRepository $categoryRepository;

    public function __construct(LoggerInterface $logger, SpeechRepository $repository, SpeechCategoryRepository $categoryRepository)
    {
        $this->logger = $logger;
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;

        // you *must* call the parent constructor
        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $csv = Reader::createFromPath(__DIR__ . '/template/liste_discours_7_2021.csv', 'r');
        $csv->setHeaderOffset(0);
        $csv->setDelimiter(';');
        /** @var array $result */
        $result = iterator_to_array($csv->getRecords());

        for ($i = 1; $i < count($result); $i++) {

            $speech = new Speech();
            $speech->setTitle($result[$i]['title']);
            $speech->setNumber($result[$i]['number']);
            $category = $this->categoryRepository->findOrCreate($result[$i]['category']);
            $speech->setCategory($category);
            $this->repository->add($speech, true);
        }

        $this->logger->info('Thèmes de discours ajoutés depuis le fichier csv');
        $io->success(count($result) . " thèmes ont été ajoutés");

        return Command::SUCCESS;
    }
}
