<?php

namespace App\Controller\Board;

use App\Entity\BoardItem;
use App\Form\BoardItemType;
use App\Repository\BoardCategoryRepository;
use App\Repository\BoardItemRepository;
use App\Service\FileUploaderService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route(path: '/board/items')]
class BoardItemController extends AbstractController
{


    public function __construct(private BoardItemRepository $repository)
    {
    }

    #[Route(path: '/add', name: 'app_board_boarditem_add')]
    public function add(Request $request, SluggerInterface $slugger, FileUploaderService $fileUploader, BoardCategoryRepository $categoryRepository): RedirectResponse|Response
    {
        $item = new BoardItem();
        $item->setCreatedBy($this->getUser());

        if ($request->query->get('categoryId')) {
            $category = $categoryRepository->find($request->query->get('categoryId'));
            if ($category) {
                $item->setCategory($category);
            }
        }

        $form = $this->createForm(BoardItemType::class, $item);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $documentFile */
            $documentFile = $form->get('document')->getData();
            if ($documentFile) {
                $documentFileName = $fileUploader->upload($documentFile);
                $item->setDocumentFileName($documentFileName);
            }

            $slugger = $slugger->withEmoji();
            $slug = $slugger->slug($item->getTitle(), '-', 'fr');
            $item->setSlug($slug);

            $this->repository->add($item, true);
            $this->addFlash('success', "L'élement {$item->getTitle()} a été ajouté au tableau d'affichage");
            return $this->redirectToRoute('app_core_index');
        }
        return $this->render('board/item/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

}