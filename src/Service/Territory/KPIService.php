<?php

namespace App\Service\Territory;

use App\Repository\Territory\AreaRepository;
use App\Repository\Territory\TerritoryRepository;

class KPIService
{

    private TerritoryRepository $territoryRepository;

    private AreaRepository $areaRepository;

    public function __construct(TerritoryRepository $territoryRepository, AreaRepository $areaRepository)
    {
        $this->territoryRepository = $territoryRepository;
        $this->areaRepository = $areaRepository;

    }


    public function getKPIs(): array
    {
        $nbTerritories = $this->territoryRepository->count([]);
        $nbTerritoriesWorked = $this->territoryRepository->countTerritoriesWorked();

        return [
            'nbTerritories' => $nbTerritories,
            'nbTerritoriesWorked' => $nbTerritoriesWorked,
            'percentWorked' => number_format(($nbTerritoriesWorked / $nbTerritories)*100, 1, ','),
        ];
    }

    public function areasWorkedTotally()
    {
        $areasWorked = $this->areaRepository->findForExportS13();
        $areas = $this->areaRepository->findAll();

        foreach ($areasWorked as $worked) {
            foreach ($areas as $area) {
                if ($area->getId() === $worked->getId()) {
                    $area->setTerritoriesWorked($worked->getTerritories());
                }
            }
        }
        return $areas;
    }

  public function s13()
  {
    $areasWorked = $this->areaRepository->findForExportS13();
    return $areasWorked;
  }

}
