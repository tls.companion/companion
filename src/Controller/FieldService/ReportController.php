<?php

namespace App\Controller\FieldService;

use App\Entity\FieldService\Report;
use App\Entity\Publisher;
use App\Form\FieldService\ReportType;
use App\Repository\FieldService\ReportRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/field_service/reports')]
class ReportController extends AbstractController
{
    #[Route(path: '/create/publisher/{publisher}/{month}/{year}', name: 'app_fieldservice_report_create')]
    public function create(Publisher $publisher, int $month, int $year, Request $request, ReportRepository $repository)
    {
        $report = new Report();
        $report
            ->setDate(new \DateTime("{$year}-{$month}-01"))
            ->setPublisher($publisher)
            ->setCreatedBy($this->getUser())
            ;
        $form = $this->createForm(ReportType::class, $report);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($report, true);
            $this->addFlash('success',"Le rapport d'activité de prédication de {$publisher->getFullName()} a bien été enregistré.");
            return $this->redirectToRoute('app_fieldservice_report_create');
        }
        return $this->render('field_service/report/create.html.twig', [
            'form' => $form->createView(),
            'report' => $report
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_fieldservice_report_edit')]
    public function edit(Report $report, Request $request, ReportRepository $repository)
    {
        $form = $this->createForm(ReportType::class, $report);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($report, true);
            $this->addFlash('success',"Le rapport d'activité de prédication de {$report->getPublisher()->getFullName()} a bien été modifié.");
            return $this->redirectToRoute('app_publisher_show', ['id' => $report->getPublisher()->getId()]);
        }
        return $this->render('field_service/report/edit.html.twig', [
            'form' => $form->createView(),
            'report' => $report
        ]);
    }
}
