<?php

namespace App\Repository\Meeting\Weekend;

use App\Entity\Meeting\Weekend\SpeechCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<SpeechCategory>
 *
 * @method SpeechCategory|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpeechCategory|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpeechCategory[]    findAll()
 * @method SpeechCategory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpeechCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpeechCategory::class);
    }

    public function add(SpeechCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(SpeechCategory $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findOrCreate(string $title)
    {
        $title = trim(strtolower($title));
        $result = $this->createQueryBuilder('c')
            ->where('c.title = :title')
            ->setParameter('title', $title)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$result) {
            $category = new SpeechCategory();
            $category->setTitle($title);
            $this->add($category, true);
            return $category;
        }

        return $result;

    }
}
