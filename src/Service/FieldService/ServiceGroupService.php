<?php

namespace App\Service\FieldService;

use App\Entity\FieldService\Report;
use App\Entity\FieldService\ReportCompilation;
use App\Entity\FieldService\ServiceGroup;
use App\Entity\User;
use App\Repository\FieldService\ReportCompilationRepository;
use App\Repository\FieldService\ReportRepository;
use App\Repository\FieldService\ServiceGroupRepository;
use App\Repository\PublisherRepository;
use App\Repository\Territory\HistoryRepository;

class ServiceGroupService
{
    private ServiceGroupRepository $repository;
    private ReportCompilationRepository $compilationRepository;
    private PublisherRepository $publisherRepository;
    private ReportRepository $reportRepository;

    private HistoryRepository $historyRepository;

    public function __construct(ServiceGroupRepository $repository,
                                ReportCompilationRepository $compilationRepository,
                                ReportRepository $reportRepository,
                                PublisherRepository $publisherRepository,
                                HistoryRepository $historyRepository
    )
    {
        $this->repository = $repository;
        $this->compilationRepository = $compilationRepository;
        $this->publisherRepository = $publisherRepository;
        $this->reportRepository = $reportRepository;
        $this->historyRepository = $historyRepository;
    }


    public function create(ServiceGroup $serviceGroup): ServiceGroup
    {
        $this->repository->add($serviceGroup, true);
        foreach ($serviceGroup->getPublishers() as $publisher) {
            foreach ($publisher->getReports() as $report) {
                $compilation = $this->compilationRepository->findOrCreate($report->getDate(), $serviceGroup);
                $compilation->addReport($report);
                $this->compilationRepository->add($compilation, true);
            }
        }
        return $serviceGroup;
    }

    public function list(): array
    {
        return $this->repository->findAll();
    }

    public function show(int $id): ServiceGroup
    {
        return $this->repository->listOrderByPublisher($id);
    }

    public function searchForShow(int $idGroup): array
    {
        $publishers = $this->publisherRepository->findForGroupShow($idGroup);
        $compilations = $this->compilationRepository->searchEngine($idGroup);

        return [
            'publishers' => $publishers,
            'compilations' => $compilations,
        ];
    }

    public function checkMonth(ServiceGroup $group, User $user): ServiceGroup
    {
        $currentMonth = new \DateTime("first day of now");
        $monthToCompile = $this->compilationRepository->checkMonthToCompile($group);
        if ($monthToCompile) {
            $compilation = new ReportCompilation();
            foreach ($group->getPublishers() as $publisher) {
                $report = new Report();
                $report->setDate($currentMonth)
                    ->setPublisher($publisher)
                    ->setCreatedBy($user)
                    ;
                $compilation->addReport($report);
            }
            $compilation->setDate($currentMonth);
            $group->addReportCompilation($compilation);
            $this->repository->add($group, true);
        }
        return $group;
    }

    public function getTerriroriesHistoryByGroup(int $idGroup)
    {
        return $this->historyRepository->findByServiceGroup($idGroup);
    }

}
