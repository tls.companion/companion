<?php

namespace App\Repository\FieldService;

use App\Entity\FieldService\ServiceGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ServiceGroup>
 *
 * @method ServiceGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServiceGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServiceGroup[]    findAll()
 * @method ServiceGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServiceGroupRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServiceGroup::class);
    }

    public function add(ServiceGroup $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ServiceGroup $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function listOrderByPublisher(int $id): ServiceGroup
    {
        return $this->createQueryBuilder('sg')
            ->addSelect('p')
            ->where('sg.id = :id')
            ->setParameter('id', $id)
            ->leftJoin('sg.publishers', 'p')
            ->orderBy('p.lastName', 'ASC')
            ->getQuery()
            ->getSingleResult();
    }

    public function listForReportCreation(int $id, \DateTime $date): ?ServiceGroup
    {
        return $this->createQueryBuilder('sg')
            ->addSelect('p')
            ->addSelect('r')
            ->where('sg.id = :id')
            ->leftJoin('sg.publishers', 'p')
            ->leftJoin('sg.reports', 'r')
            ->andWhere('r.date = :date')
            ->orderBy('p.lastName', 'ASC')
            ->setParameters(['date' => $date, 'id' => $id])
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findForReportList()
    {
        return $this->createQueryBuilder('sg')
            ->addSelect('r')
            ->leftJoin('sg.reports', 'r')
            ->groupBy('r.date')
            ->orderBy('sg.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function searchEngine()
    {


        $dates = [];
        for ($i = 1; $i < 7; $i++) {
            $dates[] = (new \DateTime("first day of -$i month"))->format('Y-m-d');
        }

        $results =  $this->createQueryBuilder('sg')
            ->addSelect('rc')
            ->leftJoin('sg.reportCompilations', 'rc')
            ->where('rc.date IN (:dates)')
            ->setParameters([
                'dates' => $dates
            ])
            ->orderBy('sg.name', 'ASC')
            ->getQuery()
            ->getResult();

        dd($results);
    }


}
