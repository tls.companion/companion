<?php

namespace App\Form\Meeting\Weekend;

use App\Entity\Meeting\Weekend\Speech;
use App\Entity\Meeting\Weekend\SpeechCategory;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpeechType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre'
            ])
            ->add('number', NumberType::class, [
                'required' => false,
                'label' => 'Numéro'
            ])
            ->add('category', EntityType::class, [
                'class' => SpeechCategory::class,
                'choice_label' => 'title',
                'required' => false,
                'label' => 'Catégorie'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Speech::class,
        ]);
    }
}
