<?php

namespace App\Controller;

use App\Repository\PublisherRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/weekeend/assignments')]
class WeekendAssignmentController extends AbstractController
{

    /**
     * @param PublisherRepository $publisherRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    #[Route(name: 'app_weekend_weekend_assignment_list')]
    public function list(PublisherRepository $publisherRepository)
    {
        return $this->render('weekend/assignment/list.html.twig',[
            'publishers' => $publisherRepository->findAllForAssignment(true),
        ]);
    }

}
