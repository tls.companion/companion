<?php

namespace App\Controller\Territory;

use App\Entity\Territory\Area;
use App\Entity\Territory\History;
use App\Entity\Territory\Territory;
use App\Form\Territory\AreaType;
use App\Form\Territory\HistoryType;
use App\Form\Territory\TerritoryType;
use App\Repository\Territory\AreaRepository;
use App\Repository\Territory\HistoryRepository;
use App\Repository\Territory\TerritoryRepository;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/territories')]
class TerritoryController extends AbstractController
{

    #[Route(path: '/areas', name: 'app_territory_areas_list')]
    public function listAreas(AreaRepository $repository, TerritoryRepository $territoryRepository)
    {
        return $this->render('territory/area/list.html.twig', [
            'areas' => $repository->findAllForList(),
            'count' => $territoryRepository->count([])
        ]);
    }

    #[Route(path: '/areas/add', name: 'app_territory_areas_add')]
    public function addArea(Request $request, AreaRepository $repository)
    {
        $area = new Area();
        $form = $this->createForm(AreaType::class, $area);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($area, true);
            $this->addFlash('success', 'La zone a été créée');
            if ($request->query->get('createNext')) {
                return $this->redirectToRoute('app_territory_areas_add');
            }
            return $this->redirectToRoute('app_territory_areas_show', [
                'area' => $area->getId()
            ]);
        }

        return $this->render('territory/area/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/add', name: 'app_territory_add')]
    public function addTerritory(Request $request, TerritoryRepository $repository)
    {
        $territory = new Territory();
        $form = $this->createForm(TerritoryType::class, $territory);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($territory, true);
            $this->addFlash('success', 'Le territoire a été créé');
            if ($request->query->get('createNext')) {
                return $this->redirectToRoute('app_territory_add');
            }
            return $this->redirectToRoute('app_territory_show', [
                'territory' => $territory->getId()
            ]);
        }

        return $this->render('territory/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/history/add/{territory}', name: 'app_territory_history_add')]
    public function addHistory(Territory $territory, Request $request, HistoryRepository $repository)
    {
        $history = new History();
        $lastHistory = $request->query->get('history');
        if ($lastHistory) {
            $history = $repository->find($lastHistory);
        }

        $history->setTerritory($territory);
        $history->setCreatedAt(new \DateTimeImmutable());
        $history->setCreatedBy($this->getUser());
        $form = $this->createForm(HistoryType::class, $history);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $territoriesToClose = $request->request->all('territoriesToClose');
            if (count($territoriesToClose) > 0) {
                foreach ($territoriesToClose as $idHistory) {
                    $historyToSetAsClosed = $repository->findOneBy(['id' => $idHistory]);
                    $historyToSetAsClosed->setInDate(new \DateTime());
                    $repository->add($historyToSetAsClosed, true);
                }
            }
            $repository->add($history, true);
            $this->addFlash('success', "L'historique du territoire a été mis à jour");
            return $this->redirectToRoute('app_territory_show', [
                'territory' => $territory->getId()
            ]);
        }

        return $this->render('territory/history/create.html.twig', [
            'form' => $form->createView(),
            'territory' => $territory
        ]);
    }

    #[Route(path: '/areas/show/{area}', name: 'app_territory_areas_show')]
    public function showArea(int $area, Request $request, AreaRepository $repository)
    {
        $countDueTerritories = 0;
        $filterRequest = $request->query->get('filter');
        $duesTerritories = [];
        $availableTerritories = [];
        $area = $repository->findByIdForShow($area);
        foreach ($area->getTerritories() as $territory) {
            foreach ($territory->getHistories() as $history) {
                $nextTwoMonth = new \DateTimeImmutable($history->getOutDate()->format('Y-m-d') . ' +3months');
                $now = new \DateTimeImmutable('now');
                if (!$history->getInDate() && ($nextTwoMonth->format('U') < $now->format('U'))) {
                    $countDueTerritories++;
                    $duesTerritories[] = $territory;
                }

                $lastIndex = $territory->getHistories()->count()-1;
                $lastHistory = $territory->getHistories()->get($lastIndex);
                if ($history === $lastHistory && $history->getInDate()){
                    $nextSixMonth = new \DateTimeImmutable($history->getInDate()->format('Y-m-d') . ' +6months');
                    if ($nextSixMonth->format('U') < $now->format('U')) {
                        $availableTerritories[] = $territory;
                    }
                }
            }
            if ($territory->getHistories()->count() === 0) {
                $availableTerritories[] = $territory;
            }
        }

        if ($filterRequest === 'dues') {
            $area->resetTerritories();
            foreach ($duesTerritories as $territory)  {
                $area->addTerritory($territory);
            }
        }

        if ($filterRequest === 'available') {
            $area->resetTerritories();
            foreach ($availableTerritories as $territory)  {
                $area->addTerritory($territory);
            }
        }



        return $this->render('territory/area/show.html.twig', [
            'area' => $area,
            'countDueTerritories' => $countDueTerritories
        ]);
    }


    #[Route(path: '/show/{territory}', name: 'app_territory_show')]
    public function showTerritory(int $territory, TerritoryRepository $repository): Response
    {
        return $this->render('territory/show.html.twig', [
            'territory' => $repository->findTerritoryForShow($territory)
        ]);
    }

    #[Route(path: '/histories/edit/{history}', name: 'app_territory_history_edit')]
    public function editHistory(History $history, Request $request, HistoryRepository $repository)
    {
        $form = $this->createForm(HistoryType::class, $history);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($history, true);
            $this->addFlash('success', "L'historique du territoire a été mis à jour");
            return $this->redirectToRoute('app_territory_show', [
                'territory' => $history->getTerritory()->getId()
            ]);
        }

        return $this->render('territory/history/create.html.twig', [
            'form' => $form->createView(),
            'territory' => $history->getTerritory(),
        ]);
    }

    #[Route(path: '/histories/remove/{history}', name: 'app_territory_history_remove')]
    public function removeHistory(History $history, HistoryRepository $repository, Request $request)
    {
        $repository->remove($history, true);
        $this->addFlash('success', 'Supprimé');
        $route = $request->headers->get('referer');

        return $this->redirect($route);
    }

    #[Route(path: '/remove/{territory}', name: 'app_territory_remove')]
    public function removeTerritory(Territory $territory, TerritoryRepository $repository, Request $request)
    {
        $repository->remove($territory, true);
        $this->addFlash('success', 'Supprimé');
        $route = $request->headers->get('referer');

        return $this->redirect($route);
    }

    #[Route(path: '/check/{publisher}', name: 'app_territory_check_territory')]
    public function checkWorkingTerritories(int $publisher, HistoryRepository $historyRepository)
    {
        return $this->render('territory/history/fragments/_history_dues_publisher.html.twig', [
            'histories' => $historyRepository->findByPublisher($publisher)
        ]);
    }
}
