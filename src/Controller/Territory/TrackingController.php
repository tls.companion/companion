<?php

namespace App\Controller\Territory;

use App\Entity\User;
use App\Repository\Territory\HistoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: "/territories/tracking")]
class TrackingController extends AbstractController
{

  #[Route(path: "/responsible/{user}", name: "app_territory_tracking")]
  public function tracking(User $user, HistoryRepository $repository): Response
  {
    return $this->render('territory/tracking/tracking.html.twig', [
      'user' => $user,
      'histories' => $repository->findCreatedByUserForTracking($user->getId())
    ]);
  }

}
