<?php

namespace App\Form\Territory;

use App\Entity\Territory\Area;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AreaType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'required' => true,
                'label' => 'Nom de la zone',
                'attr' => [
                    'placeholder' => 'ex : Purpan'
                ],
            ])
            ->add('cloudLink', TextType::class, [
                'required' => false,
                'attr' => [
                    'placeholder' => 'Lien Google Drive / One Drive'
                ],
                'label' => 'Lien'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Area::class,
        ]);
    }
}
