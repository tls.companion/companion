<?php

namespace App\Controller\FieldService;

use App\Entity\FieldService\Report;
use App\Entity\FieldService\ReportCompilation;
use App\Entity\FieldService\ServiceGroup;
use App\Entity\Publisher;
use App\Exception\UserNotGrantedException;
use App\Form\FieldService\ReportCompilationType;
use App\Repository\FieldService\ReportCompilationRepository;
use App\Repository\FieldService\ServiceGroupRepository;
use App\Repository\UserRepository;
use App\Service\FieldService\ReportCompilationService;
use App\Service\MailerService;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/field_service/compilation')]
class ReportCompilationController extends AbstractController
{

    private ReportCompilationService $service;
    private LoggerInterface $logger;

    /**
     * @param ReportCompilationService $service
     */
    public function __construct(ReportCompilationService $service, LoggerInterface $logger)
    {
        $this->service = $service;
        $this->logger = $logger;
    }


    #[Route(path: '/create/group/{group}/{month}/{year}', name: 'app_fieldservice_reportcompilation_create')]
    public function create(ServiceGroup $group, int $month, int $year, Request $request, MailerService $mailer, UserRepository $userRepository): Response
    {
        $publisher = $this->getUser()->getPublisher();
        if ((!$this->isGranted('ROLE_SERVICE_COMMITTEE')) && $publisher !== $group->getOverseer() && $publisher !== $group->getAssistant() && !$publisher->isIsElder()) {
            throw new UserNotGrantedException();
        }

        $date = new \DateTime("{$year}-{$month}-01");
        $reportCompilation =$this->service->prepareCreate($date, $group, $this->getUser());
        $form = $this->createForm(ReportCompilationType::class, $reportCompilation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->service->createOrUpdate($reportCompilation);
            if ($reportCompilation->isClosed()) {
                $secretary = $userRepository->findSecretaryOrSuperAdmin();
                if ($secretary) {
                    $context = [
                        'user' => $secretary,
                        'comp' => $reportCompilation,
                        'path' => $request->getUri(),
                    ];
                    $dateFormatStr = $reportCompilation->getDate()->format('F Y');
                    $subject = $group->getName() . " | Les rapports du mois de $dateFormatStr ont été cloturé.";
                    $template = 'emails/field_service/compilation_closed.html.twig';
                    $mailer->send($secretary, $subject, $template, $context);
                } else {
                    $this->logger->error('Aucun secrétaire ou Administrateur de haut niveau n\a été trouvé. La notification n\'a pas été envoyée.');
                }

            }
            $this->addFlash('success', 'Les rapports ont été enregistrés.');
            return $this->redirectToRoute('app_fieldservice_servicegroup_show', ['id' => $group->getId()]);
        }

        return $this->render('field_service/compilation/create.html.twig', [
            'form' => $form->createView(),
            'compilation' => $reportCompilation
        ]);

    }

    #[Route(path: '/list', name: 'app_fieldservice_reportcompilation_list')]
    public function list(ServiceGroupRepository $groupRepository)
    {
        /** @var Publisher|null $publisher */
        $publisher = $this->getUser()->getPublisher();
        $myGroup = null;
        if($publisher) {
            $myGroup = $publisher->getServiceGroup();
        }

        $currentDate = new \DateTime("first day of now");

        return $this->render('field_service/compilation/list.html.twig', [
            'groups' => $groupRepository->findAll(),
            'myGroup' => $myGroup,
            'currentDate' => $currentDate
        ]);
    }

}
