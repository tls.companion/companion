<?php

namespace App\Repository;

use App\Entity\Publisher;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Publisher>
 *
 * @method Publisher|null find($id, $lockMode = null, $lockVersion = null)
 * @method Publisher|null findOneBy(array $criteria, array $orderBy = null)
 * @method Publisher[]    findAll()
 * @method Publisher[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PublisherRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Publisher::class);
    }

    public function add(Publisher $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Publisher $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findForGroupShow($idGroup)
    {
        return $this->createQueryBuilder('p')
            ->leftJoin('p.serviceGroup', 'sg')
            ->where('sg.id = :idGroup')
            ->leftJoin('p.reports', 'r')
            ->andWhere('p.deletedAt IS NULL')
            ->andWhere('p.disabled = 0')
            ->setParameter('idGroup', $idGroup)
            ->addSelect('r')
            ->orderBy('p.lastName', 'ASC')
            ->getQuery()->getResult()
        ;
    }

    public function findAllForAssignment($isSpeaker = false)
    {
        $qb =  $this->createQueryBuilder('p')
            ->addSelect('a')
            ->where('p.gender = :gender')
            ->setParameter('gender', 'M')
            ->leftJoin('p.assignment', 'a')
            ->andWhere('a IS NOT NULL')
        ;
        if ($isSpeaker) {
          $qb->andWhere('a.speaker = 1');
        }
        $qb
            ->orderBy('p.lastName', 'ASC')
            ->getQuery()
            ->getResult()
            ;

        return $qb
          ->getQuery()
          ->getResult()
          ;
    }

    public function findForWeekendProgramForm(string $type): QueryBuilder
    {
        $em = $this->getEntityManager();
        $expr = $em->getExpressionBuilder();
        $qb = $this->createQueryBuilder('p')
            ->leftJoin('p.assignment', 'a')
            ->where('p.disabled IS NULL')
            ->orWhere('p.disabled = 0')
            ->andWhere('p.deletedAt IS NULL')
        ;

        if ($type === 'speaker') {
            $qb->andWhere(
                $expr->eq('pw.id', "(" . $em->createQueryBuilder()->select('MAX(w.id)')->from('App:Meeting\Weekend\Weekend', 'w')
                        ->where('w.speaker = p')
                        ->andWhere($expr->eq('w.date', "(". $em->createQueryBuilder()->select('MAX(w.date)')
                                ->from('App:Meeting\Weekend\Weekend', 'we') . ")"))->orderBy('w.date', 'DESC')) . ")");
            $qb->orWhere($expr->isNull("pw.id"));
            $qb->leftJoin('p.speakerWeekends', 'pw')
                ->andWhere("a.speaker = 1")
            ;
        }

        if ($type === 'chairman') {
            $qb->andWhere(
                $expr->eq('pw.id', "(" . $em->createQueryBuilder()->select('MAX(w.id)')->from('App:Meeting\Weekend\Weekend', 'w')
                        ->where('w.chairman = p')
                        ->andWhere($expr->eq('w.date', "(". $em->createQueryBuilder()->select('MAX(w.date)')
                                ->from('App:Meeting\Weekend\Weekend', 'we') . ")"))->orderBy('w.date', 'DESC')) . ")");
            $qb->orWhere($expr->isNull("pw.id"));
            $qb->leftJoin('p.chairmanWeekends', 'pw')
                ->andWhere("a.weekendChairman = 1")
            ;
        }

        if ($type === 'reader') {
            $qb->andWhere(
                $expr->eq('pw.id', "(" . $em->createQueryBuilder()->select('MAX(w.id)')->from('App:Meeting\Weekend\Weekend', 'w')
                        ->where('w.reader = p')
                        ->andWhere($expr->eq('w.date', "(". $em->createQueryBuilder()->select('MAX(w.date)')
                                ->from('App:Meeting\Weekend\Weekend', 'we') . ")"))->orderBy('w.date', 'DESC')) . ")");
            $qb->orWhere($expr->isNull("pw.id"));
            $qb->leftJoin('p.readerWeekends', 'pw')
                ->andWhere("a.watchtowerReader = 1")
            ;
        }

        return $qb->orderBy('pw.date', 'ASC');


    }

    public function autocomplete(?string $term)
    {
        $qb = $this->createQueryBuilder('p')
          ->where('p.disabled = 0 AND p.deletedAt IS NULL')
            ;

        if ($term){
            $qb
              ->andWhere('p.firstName LIKE :term OR p.lastName LIKE :term')
              ->setParameter('term', "%{$term}%")
            ;
        }

        return $qb
            ->orderBy('p.lastName', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

  public function findAllSpeakersForSurveyGeneration()
  {
    return $this->createQueryBuilder('p')
      ->leftJoin('p.assignment', 'a')
      ->where('a.speaker = :speaker')
      //->andWhere('speech_surveys.year != :year')
      ->setParameters([
        'speaker' => '1',
        //'year' => 2025
      ])
      ->getQuery()
      ->getResult();
  }
}
