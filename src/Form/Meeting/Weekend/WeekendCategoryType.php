<?php

namespace App\Form\Meeting\Weekend;

use App\Entity\Meeting\Weekend\WeekendCategory;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WeekendCategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'Nom',
                'required' => true,
                'attr' => [
                    'placeholder' => 'ex: Assemblée régionale - Soyez patients !'
                ]
            ])
            ->add('chairmanEnabled', null, [
                'label' => 'Président'
            ])
            ->add('readerEnabled', null, [
                'label' => 'Lecteur Tour de Garde'
            ])
            ->add('speakerEnabled', null, [
                'label' => 'Orateur'
            ])
            ->add('externalSpeakerEnabled', null, [
                'label' => 'Orateur externe'
            ])
            ->add('speechEnabled', null, [
                'label' => 'Discours depuis s-99'
            ])
            ->add('overseerVisitEnabled', null, [
                'label' => 'Semaine spéciale'
            ])
            ->add('conventionEnabled', null, [
                'label' => 'Week-end d\'assemblée'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => WeekendCategory::class,
        ]);
    }
}
