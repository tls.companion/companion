<?php

namespace App\Entity\Territory;

use App\Repository\Territory\TerritoryRepository;
use App\Service\Utils\Territory\AvailabilityCalculation;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TerritoryRepository::class)]
class Territory
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 64)]
    private $title;

    #[ORM\Column(type: 'text', nullable: true)]
    private $cloudLink;

    #[ORM\ManyToOne(targetEntity: Area::class, inversedBy: 'territories')]
    private $area;

    #[ORM\OneToMany(targetEntity: History::class, mappedBy: 'territory', orphanRemoval: true)]
    private $histories;

    #[ORM\Column(type: 'string', nullable: true, length: 255)]
    private ?string $prefixFolder;

    public function __construct()
    {
        $this->histories = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCloudLink(): ?string
    {
        return $this->cloudLink;
    }

    public function setCloudLink(?string $cloudLink): self
    {
        $this->cloudLink = $cloudLink;

        return $this;
    }

    public function getArea(): ?Area
    {
        return $this->area;
    }

    public function setArea(?Area $area): self
    {
        $this->area = $area;

        return $this;
    }

    /**
     * @return Collection<int, History>
     */
    public function getHistories(): Collection
    {
        return $this->histories;
    }

    public function addHistory(History $history): self
    {
        if (!$this->histories->contains($history)) {
            $this->histories[] = $history;
            $history->setTerritory($this);
        }

        return $this;
    }

    public function removeHistory(History $history): self
    {
        if ($this->histories->removeElement($history)) {
            // set the owning side to null (unless already changed)
            if ($history->getTerritory() === $this) {
                $history->setTerritory(null);
            }
        }

        return $this;
    }

    public function getPrefixFolder(): ?string
    {
        return $this->prefixFolder;
    }

    public function setPrefixFolder(?string $prefixFolder): static
    {
        $this->prefixFolder = $prefixFolder;

        return $this;
    }

  public function sortHistories(): self
  {
    // Copie les historiques dans un tableau
    $histories = $this->histories->toArray();

    // Trie les historiques par la date "inDate"
    usort($histories, function($a, $b) {
      return $b->getInDate() <=> $a->getInDate();
    });

    return $this;
  }

  public function getAvailabilityStatus(): HistoryAvailabilityStatus
  {
    $availabilityCalculation = new AvailabilityCalculation();
    $this->sortHistories();
    $length = $this->histories->count();
    if ($length > 0) {
      /** @var History $last */
      $last = $this->histories->get($length-1);

      if ($availabilityCalculation->isExpired($last->getOutDate()) && !$last->getInDate()) {
        return HistoryAvailabilityStatus::EXPIRED;
      }

      if (!$last->getInDate()) {
        return HistoryAvailabilityStatus::IN_PROGRESS;
      }

      if (!$availabilityCalculation->isNowAvailable($last->getInDate())) {
        return HistoryAvailabilityStatus::NOT_AVAILABLE;
      }

    }

    return HistoryAvailabilityStatus::AVAILABLE;
  }

  public function getLastTerritoryIn(): ?History
  {
    $histories = $this->getHistories();

    if ($histories->isEmpty()) {
      return null;
    }

    $latestHistory = null;
    foreach ($histories as $history) {
      if (($latestHistory === null && $history->getInDate() !== null) || ($history->getInDate() !== null && $history->getInDate() > $latestHistory->getInDate())) {
        $latestHistory = $history;
      }
    }

    return $latestHistory;
  }
}
