<?php

namespace App\Controller\Meeting\Weekend;

use App\Entity\Meeting\Weekend\SpeechSurvey;
use App\Entity\Publisher;
use App\Form\Meeting\Weekend\SpeechSurveyType;
use App\Repository\AssignmentRepository;
use App\Repository\Meeting\SpeechSurveyRepository;
use App\Repository\Meeting\Weekend\SpeechRepository;
use App\Repository\PublisherRepository;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Attribute\Route;

#[Route('/meeting/weekend/speech/surveys')]
class SpeechSurveyController extends AbstractController
{

  public function __construct(
    private readonly SpeechSurveyRepository $repository,
    private readonly EntityManagerInterface $em
  ){}

  #[Route('/add/{token}', name: 'app_meeting_weekend_speech_survey_add')]
  public function addOrUpdate(?string $token, Request $request): Response
  {
      $survey = $this->repository->findOneBy(['token' => $token]);
      if (!$survey) {
        throw new NotFoundHttpException("Le formulaire n'a pas été trouvé");
      }
      $form = $this->createForm(SpeechSurveyType::class, $survey);
      $form->handleRequest($request);
      if ($form->isSubmitted() && $form->isValid()) {
        $this->em->persist($survey);
        $this->em->flush();
        $this->addFlash('success', "Le formulaire a été transmis avec succès. Merci pour ta participation.");
        return $this->redirectToRoute("app_meeting_weekend_speech_survey_add", ['token' => $token]);
      }
      return $this->render('meeting/weekend/speech_survey/add.html.twig', [
          'form' => $form->createView(),
          'speaker' => $survey->getSpeaker()
      ]);
  }

  #[Route('/generate/{idPublisher}', name: 'app_meeting_weekend_speech_survey_generate')]
  public function generate(Publisher $idPublisher): RedirectResponse
  {
    $token = Uuid::uuid4();
    $survey = new SpeechSurvey();
    $survey->setToken($token)
      ->setSpeaker($idPublisher)
      ;
    $survey->setYear(2025);
    $this->em->persist($survey);
    $this->em->flush();

    $this->addFlash('success', "Le formulaire pour " . $idPublisher->getFullName() . " a été généré");
    return $this->redirectToRoute('app_meeting_weekend_speech_survey_list', ['year' => 2025]);
  }

  #[Route('/{year}', name: 'app_meeting_weekend_speech_survey_list')]
  public function list(int $year, PublisherRepository $publisherRepository): Response
  {
    $speakers = $publisherRepository->findAllSpeakersForSurveyGeneration();
    return $this->render('meeting/weekend/speech_survey/list.html.twig', [
      'surveys' => $this->repository->findAllForList($year),
      'speakers' => $speakers
    ]);
  }

  #[Route('/resume')]
  public function resume(SpeechRepository $speechRepository, Request $request, PublisherRepository $publisherRepository): Response
  {
    $speakers = $publisherRepository->findAllForAssignment(true);
    $speeches = $speechRepository->findPicked();
    $publisherId = $request->query->get('publisherId');
    if ($publisherId) {
      $publisher = $publisherRepository->find($publisherId);
      $surveys = $publisher->getSpeechSurveys();
      if ($surveys->count() > 0) {
        $speeches = $surveys->get(0)->getSpeeches();
      } else {
        $speeches = [];
      }
    }
    return $this->render('meeting/weekend/speech_survey/resume.html.twig', [
      'speeches' => $speeches,
      'speakers' => $speakers
    ]);
  }


}
