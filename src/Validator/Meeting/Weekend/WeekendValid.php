<?php

namespace App\Validator\Meeting\Weekend;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class WeekendValid extends Constraint
{

    public $externalNameMsg = "Nom de l'orateur externe manquant";
    public $speakerMsg = "Orateur manquant";
    public $notBlank = "Ce champ ne peut pas être vide";
    public $dateBlank = "Merci de remplir une date valide";


    public function validatedBy()
    {
        return static::class . 'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
