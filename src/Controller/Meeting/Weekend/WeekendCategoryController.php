<?php

namespace App\Controller\Meeting\Weekend;

use App\Entity\Meeting\Weekend\WeekendCategory;
use App\Form\Meeting\Weekend\WeekendCategoryType;
use App\Repository\Meeting\Weekend\WeekendCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/meeting/weekend/categories')]
class WeekendCategoryController extends AbstractController
{

    #[Route(path: '/create', name: 'app_meeting_weekend_weekendcategory_create')]
    public function create(Request $request, WeekendCategoryRepository $repository)
    {
        $weekendCategory = new WeekendCategory();
        $form = $this->createForm(WeekendCategoryType::class, $weekendCategory);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $repository->add($weekendCategory, true);
            $this->addFlash('success', 'La catégorie a été ajoutée.');
            return $this->redirectToRoute('app_meeting_weekend_weekendcategory_create');
        }
        return $this->render('meeting/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(name: 'app_meeting_weekend_weekendcategory_list')]
    public function list(WeekendCategoryRepository $repository)
    {
        return $this->render('meeting/list.html.twig', [
            'categories' => $repository->findBy(['deletedAt' => null])
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_meeting_weekend_weekendcategory_edit')]
    public function edit(WeekendCategory $weekendCategory, Request $request, WeekendCategoryRepository $repository)
    {
        $form = $this->createForm(WeekendCategoryType::class, $weekendCategory);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $repository->add($weekendCategory, true);
            $this->addFlash('success', 'La catégorie a été modifiée.');
            return $this->redirectToRoute('app_meeting_weekend_weekendcategory_list');
        }
        return $this->render('meeting/modal_edit.html.twig', [
            'form' => $form->createView(),
            'category' => $weekendCategory,

        ]);
    }

    #[Route(path: '/show/{id}')]
    public function show(WeekendCategory $category, SerializerInterface $serializer)
    {
        $json = $serializer->serialize($category, 'json', [
            'groups' => ['show']
        ]);
        return new Response($json, Response::HTTP_OK);
    }

    #[Route(path: '/delete/{id}', name: 'app_meeting_weekend_weekendcategory_delete')]
    public function delete(WeekendCategory $category, WeekendCategoryRepository $repository)
    {
        $category->setDeletedAt(new \DateTimeImmutable());
        $repository->add($category, true);
        $this->addFlash('success', 'La catégorie a été supprimée.');
        return $this->redirectToRoute('app_meeting_weekend_weekendcategory_list');
    }

}
