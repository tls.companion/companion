<?php

namespace App\Controller\FieldService;

use App\Entity\FieldService\ServiceGroup;
use App\Entity\Publisher;
use App\Form\FieldService\EditServiceGroupType;
use App\Form\FieldService\ServiceGroupType;
use App\Form\PublisherType;
use App\Repository\FieldService\ServiceGroupRepository;
use App\Service\FieldService\ServiceGroupService;
use App\Service\PublisherService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

#[Route(path: '/field_service/groups')]
class ServiceGroupController extends AbstractController
{
    private ServiceGroupService $service;

    public function __construct(ServiceGroupService $service)
    {
        $this->service = $service;
    }

    /**
     * @IsGranted("ROLE_SERVICE_COMMITTEE")
     */
    #[Route(path: '/create', name: 'app_fieldservice_servicegroup_create')]
    public function create(Request $request): Response
    {
        $serviceGroup = new ServiceGroup();
        $form = $this->createForm(ServiceGroupType::class, $serviceGroup);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
           $this->service->create($serviceGroup);
           $this->addFlash('success', 'Le groupe de prédication ' . $serviceGroup->getName() . ' a été créé.');
           return $this->redirectToRoute('app_fieldservice_servicegroup_list');
        }
        return $this->render('field_service/service_group/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(name: 'app_fieldservice_servicegroup_list')]
    public function list()
    {
        return $this->render('field_service/service_group/list.html.twig', [
            'groups' => $this->service->list()
        ]);
    }

    
    #[Route(path: '/show/{id}', name: 'app_fieldservice_servicegroup_show')]
    public function show(ServiceGroup $group, Request $request, PublisherService $publisherService, SerializerInterface $serializer)
    {
        $group = $this->service->checkMonth($group, $this->getUser());
        $slotRequest = $request->query->get('slot');
        $typeRequest = $request->query->get('type');
        $searchEngine = $this->service->searchForShow($group->getId(), $slotRequest, $typeRequest);
        $compilations = $searchEngine['compilations'];
        $publishers = $searchEngine['publishers'];

        $publisher = new Publisher();
        $formPublisher = $this->createForm(PublisherType::class, $publisher);
        $formServiceGroup = $this->createForm(EditServiceGroupType::class, $group);
        $formPublisher->handleRequest($request);
        $formServiceGroup->handleRequest($request);

        if ($formServiceGroup->isSubmitted() && $formServiceGroup->isValid()){
            if (!$group->getPublishers()->contains($group->getOverseer())) {
                $group->addPublisher($group->getOverseer());
            }
            if (!$group->getPublishers()->contains($group->getAssistant())) {
                $group->addPublisher($group->getAssistant());
            }
            $this->service->create($group);
            $this->addFlash('success', 'Informations du groupe modifiés.');
            return $this->redirectToRoute('app_fieldservice_servicegroup_show', ['id' => $group->getId()]);
        }

        if ($formPublisher->isSubmitted() && $formPublisher->isValid()) {
            $publisher->setServiceGroup($group);
            $publisher = $publisherService->create($publisher);
            $this->addFlash('success', $publisher->getFullName() . ' a été ajouté(e)');
            return $this->redirectToRoute('app_fieldservice_servicegroup_show', ['id' => $group->getId()]);
        }
        return $this->render('field_service/service_group/show.html.twig', [
            'group' => $this->service->show($group->getId()),
            'formPublisher' => $formPublisher->createView(),
            'formServiceGroup' => $formServiceGroup->createView(),
            'dataCharts' => $serializer->serialize($compilations, 'json', [
                'groups' => ['group_charts']
            ]),
            'compilations' => $compilations,
            'publishers' => $publishers,
            'histories' => $this->service->getTerriroriesHistoryByGroup($group->getId())
         ]);
    }

    #[Route(path: '/delete/{id}', name: 'app_fieldservice_servicegroup_delete', methods: ['DELETE'])]
    public function delete(ServiceGroup $group, ServiceGroupRepository $repository)
    {
        $repository->remove($group, true);
        $this->addFlash('success', 'Le groupe a été supprimé.');
        return $this->redirectToRoute('app_fieldservice_servicegroup_list');
    }

}
