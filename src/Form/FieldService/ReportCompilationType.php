<?php

namespace App\Form\FieldService;

use App\Entity\FieldService\ReportCompilation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReportCompilationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('closed', null, [
                'label' => 'Cloturer la compilation'
            ])
            ->add('reports', CollectionType::class, [
                'entry_type' => ReportType::class,
                'by_reference' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ReportCompilation::class,
        ]);
    }
}
