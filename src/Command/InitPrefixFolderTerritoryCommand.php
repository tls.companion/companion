<?php

namespace App\Command;

use App\Repository\Territory\AreaRepository;
use App\Repository\Territory\TerritoryRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:init-prefix-folder-territory',
    description: 'Initialisation du prefix dossier territoire',
)]
class InitPrefixFolderTerritoryCommand extends Command
{
    private TerritoryRepository $territoryRepository;

    private AreaRepository $areaRepository;

    public function __construct(TerritoryRepository $territoryRepository, AreaRepository $areaRepository)
    {
        $this->territoryRepository = $territoryRepository;
        $this->areaRepository = $areaRepository;


        // you *must* call the parent constructor
        parent::__construct();
    }
    protected function configure(): void
    {
        $this
            ->addOption('area',  'a',InputOption::VALUE_REQUIRED, 'Area')
            ->addOption('folder',  'f',InputOption::VALUE_REQUIRED, 'Folder')

        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $areaCmd = $input->getOption('area');
        $folderCmd = $input->getOption('folder');

        $area = $this->areaRepository->findOneBy(['title' => $areaCmd]);

        if (!$area){
            $io->error('La zone ' . $areaCmd . " n'a pas été trouvée. Verifies qu'elle existe deja en base de données.");

            return Command::FAILURE;
        }

        foreach ($area->getTerritories() as $territory) {
            $territory->setPrefixFolder($folderCmd);
            $this->territoryRepository->add($territory, true);
        }

        $io->success('Les territoires de la zone ' . $areaCmd . ' ont été mis à jours.');

        return Command::SUCCESS;
    }
}
