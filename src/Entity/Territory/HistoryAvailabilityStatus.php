<?php

namespace App\Entity\Territory;

enum HistoryAvailabilityStatus: string
{
  case IN_PROGRESS = 'in_progress';
  case EXPIRED = 'expired';
  case AVAILABLE = 'available';
  case NOT_AVAILABLE = 'not_available';
}
