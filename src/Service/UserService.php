<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserService
{
    private UserRepository $repository;
    private UserPasswordHasherInterface $hasher;
    private MailerInterface $mailer;

    public function __construct(UserRepository $repository, UserPasswordHasherInterface $hasher, MailerInterface $mailer)
    {
        $this->repository = $repository;
        $this->hasher = $hasher;
        $this->mailer = $mailer;
    }

    /**
     * Create a user
     * @param User $user
     * @return User
     * @throws TransportExceptionInterface
     */
    public function create(User $user): User
    {
        $isCreation = false;
        if (!$user->getId()) {
            $isCreation = true;
            $password = $this->hasher->hashPassword($user, $_ENV['DEFAULT_USER_PASSWORD']);
            $user->setPassword($password);
        }

        $this->repository->add($user, true);

        if ($isCreation) {
            $email = (new TemplatedEmail())
                ->from($_ENV['EMAIL_FROM'])
                ->to(new Address($user->getEmail()))
                ->subject('Compte créé dans Companion !')

                // path of the Twig template to render
                ->htmlTemplate('emails/user_create.html.twig')
                ->context([
                    'user' => $user,
                ]);

            $this->mailer->send($email);
        }

        return $user;
    }

    public function list(): array
    {
        return $this->repository->findBy(['deletedAt' => null]);
    }

    public function update(User $user): User
    {
        $this->repository->add($user, true);
        return $user;
    }

    public function delete(User $user): void
    {
        $token = Uuid::uuid4()->toString();
        $user->setIsActive(false);
        $user
            ->setFirstName($token)
            ->setLastName($token)
            ->setEmail($token)
            ->setToken($token)
            ->setDeletedAt(new \DateTimeImmutable())
            ;

        $this->repository->add($user, true);
    }

}
