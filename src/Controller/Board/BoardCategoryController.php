<?php

namespace App\Controller\Board;

use App\Entity\BoardCategory;
use App\Form\BoardCategoryType;
use App\Repository\BoardCategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/board/categories')]
class BoardCategoryController extends AbstractController
{

    private BoardCategoryRepository $repository;

    public function __construct(BoardCategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    #[Route(path: '/create', name: 'app_board_boardcategory_create')]
    public function create(Request $request)
    {
       $category = new BoardCategory();
       $form = $this->createForm(BoardCategoryType::class, $category);
       $form->handleRequest($request);
       if ($form->isSubmitted() && $form->isValid()) {
           $this->repository->add($category, true);
           $this->addFlash('success', "Catégorie créée pour le tableau d'affichage");
           return $this->redirectToRoute('app_board_boardcategory_list');
       }

       return $this->render('board/category/create.html.twig', [
           'form' => $form->createView(),
       ]);
    }

    #[Route(path: '/', name: 'app_board_boardcategory_list')]
    public function list(Request $request)
    {
        return $this->render('board/category/list.html.twig', [
            'categories' => $this->repository->findAll()
        ]);
    }

}