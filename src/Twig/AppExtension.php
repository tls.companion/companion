<?php

namespace App\Twig;

use App\Entity\FieldService\ReportCompilation;
use App\Entity\Territory\History;
use App\Entity\Territory\HistoryAvailabilityStatus;
use App\Entity\Territory\Territory;
use Doctrine\Common\Collections\ArrayCollection;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{

    public function getFilters(): array
    {
        return [
            new TwigFilter('role', [$this, 'formatRole']),
            new TwigFilter('avgTimes', [$this, 'avgTime']),
            new TwigFilter('avg', [$this, 'avg']),
            new TwigFilter('totalAvg', [$this, 'getTotalAvg']),
            new TwigFilter('total', [$this, 'total']),
            new TwigFilter('minToHour', [$this, 'minToHour']),
            new TwigFilter('float', [$this, 'float']),
            new TwigFilter('territoryDateValidation', [$this, 'territoryDateValidation']),
            new TwigFilter('availabilityToString', [$this, 'availabilityToString']),
            new TwigFilter('availabilityToBadge', [$this, 'availabilityToBadge'])
        ];
    }

    public function formatRole(string $role)
    {
        if ($role === 'ROLE_SUPER_ADMIN'){
            return 'Administrateur';
        }
        if ($role === 'ROLE_COORDINATOR') {
            return 'Coordinateur du collège des anciens';
        }
        if ($role === 'ROLE_SECRETARY') {
            return 'Secrétaire';
        }
        if ($role === 'ROLE_SERVICE_OVERSEER') {
            return 'Responsable de la prédication';
        }
        if ($role === ('ROLE_PUBLIC_TALK_OVERSEER')) {
            return 'Responsable programmes des discours publics';
        }
        if ($role === ('ROLE_PUBLIC_TALK_ASSISTANT')) {
            return 'Adjoint aux programmes des discours publics';
        }
        if ($role === ('ROLE_CHRISTIAN_LIFE_MINISTRY_OVERSEER')) {
            return 'Responsable réunion VCM';
        }
        if ($role === ('ROLE_CHRISTIAN_LIFE_MINISTRY_ASSISTANT')) {
            return 'Adjoint programme réunion VCM';
        }
        if ($role === ('ROLE_ADMIN')) {
            return 'Support technique';
        }
        if ($role === ('ROLE_TERRITORY')) {
            return 'Préposé aux territoires';
        }
        return null;
    }

    public function avgTime($reports)
    {
        $nbReports = $reports->count();

        if ($nbReports == 0) {
            return "00:00";
        }

        $sum = 0;
        foreach ($reports as $report) {
            $sum += $report->getTimes();
        }
        $time = $sum / $nbReports;
        $hours = floor($time / 60);
        $minutes = $time % 60;
        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }
        return "$hours:$minutes";
    }

    public function avg($reports, string $type)
    {
        $sum = 0;
        $result = 0;
        $count = $reports->count();
        if ($count === 0) {
            return 0;
        }
        switch ($type) {
            case "visits":
                foreach ($reports as $report) {
                    $sum += $report->getVisits();
                }
                $result = $sum / $count;
                break;
            case "publications":
                foreach ($reports as $report) {
                    $sum += $report->getPublications();
                }
                $result = $sum / $count;
                break;
            case "courses":
                foreach ($reports as $report) {
                    $sum += $report->getCourses();
                }
                $result = $sum / $count;
                break;
            case "videos":
                foreach ($reports as $report) {
                    $sum += $report->getVideos();
                }
                $result = $sum / $count;
                break;
            case "otherTimes":
                foreach ($reports as $report) {
                    $sum += $report->getOtherTimes();
                }
                $times = $sum / 60;
                $result = floor($times / $count);
                break;
        }

        return $result;
    }

    /**
     * @param ReportCompilation[] $compilations
     */
    public function getTotalAvg($compilations, string $type): string
    {
        $sum = 0;
        $count = 0;
        foreach ($compilations as $compilation) {
            if($compilation->isClosed()) {
                $count++;
                if ($type === 'times') {
                    $sum += $compilation->getTimesAvg();
                } else {
                    $sum += $this->avg($compilation->getReports(), $type);
                }
            }
        }
        if ($sum === 0) {
            return 0;
        }
        $result = $sum / $count;
        if($type == ('times' || 'otherTimes')) {
            return number_format($result, 0, ',', ' ');
        }
        return number_format($result, 1, ',', ' ');
    }

    /**
     * @param ReportCompilation[]|ReportCompilation $compilations
     * @param string $type
     * @return float|int
     */
    public function total($compilations, string $type)
    {
        $total = 0;

        if ($compilations instanceof ReportCompilation) {
            $total = $this->calculTotal($compilations, $type);
        }
        else {
            foreach ($compilations as $compilation) {
                $total += $this->calculTotal($compilation, $type);
            }
        }


        if ($type === 'times') {
            return number_format(($total / 60), 0,',', ' ');
        }

        return number_format($total, 0, ',', ' ');
    }

    public function calculTotal(ReportCompilation $compilation, string $type)
    {
        $total = 0;
        if ($compilation->isClosed()) {
            foreach ($compilation->getReports() as $report) {
                if ($type === 'times') {
                    $total += $report->getTimes();
                }
                if ($type === 'visits') {
                    $total += $report->getVisits();
                }
                if ($type === 'publications') {
                    $total += $report->getPublications();
                }
                if ($type === 'courses') {
                    $total += $report->getCourses();
                }
                if ($type === 'videos') {
                    $total += $report->getVideos();
                }
                if ($type === 'otherTimes') {
                    $total += ($report->getOtherTimes() / 60);
                }
            }
        }
        return $total;
    }

    public function minToHour(?int $minutes)
    {
        if (!$minutes) {
            return 0;
        }
        return floor($minutes / 60);
    }

    public function float($val)
    {
        return floatval($val);
    }

    public function territoryDateValidation($date, $delay)
    {
        return new \DateTimeImmutable($date->format("Y-m-d") . " +" . $delay);
    }

  public function availabilityToString(HistoryAvailabilityStatus $status): string
  {
    if ($status === HistoryAvailabilityStatus::AVAILABLE) {
      return 'Disponible';
    }
    if ($status === HistoryAvailabilityStatus::IN_PROGRESS) {
      return 'En cours de travail';
    }
    if ($status === HistoryAvailabilityStatus::EXPIRED) {
      return 'À rendre';
    }
    if ($status === HistoryAvailabilityStatus::NOT_AVAILABLE) {
      return 'Délai non atteint pour sortie';
    }
    return 'Statut indisponible';
  }

  public function availabilityToBadge(HistoryAvailabilityStatus $status): string
  {
    if ($status === HistoryAvailabilityStatus::AVAILABLE) {
      return 'bg-success';
    }
    if ($status === HistoryAvailabilityStatus::IN_PROGRESS) {
      return 'bg-warning';
    }
    if ($status === HistoryAvailabilityStatus::EXPIRED) {
      return 'bg-danger';
    }
    if ($status === HistoryAvailabilityStatus::NOT_AVAILABLE) {
      return 'bg-danger';
    }
    return 'Statut indisponible';
  }

}
