<?php

namespace App\Controller;

use App\Entity\Assignment;
use App\Form\WeekendAssignmentEditType;
use App\Form\WeekendAssignmentType;
use App\Repository\AssignmentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/assignments')]
class AssignmentController extends AbstractController
{

    #[Route(path: '/create', name: 'app_assignment_create')]
    public function create(AssignmentRepository $repository, Request $request)
    {
        $assignment = new Assignment();
        $form = $this->createForm(WeekendAssignmentType::class, $assignment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($assignment, true);
            $this->addFlash('success', 'Attributions ajoutés');
            return $this->redirectToRoute('app_assignment_create');
        }

        return $this->render('weekend/assignment/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/edit/{id}', name: 'app_assignment_edit')]
    public function edit(Assignment $assignment, Request $request, AssignmentRepository $repository)
    {
        $form = $this->createForm(WeekendAssignmentEditType::class, $assignment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $repository->add($assignment, true);
            $this->addFlash('success', 'Attributions mises à jours.');
            return $this->redirectToRoute('app_weekend_weekend_assignment_list');
        }
        return $this->render('weekend/assignment/modal_edit.html.twig', [
            'publisher' => $assignment->getPublisher(),
            'form' => $form->createView(),
            'id' => $assignment->getId()
        ]);
    }

    #[Route(path: '/delete/{id}', name: 'app_assignment_delete')]
    public function delete(Assignment $assignment, AssignmentRepository $repository)
    {
        $repository->remove($assignment, true);
        $this->addFlash('success', "Les attributions ont été supprimés");
        return $this->redirectToRoute('app_weekend_weekend_assignment_list');
    }
}
