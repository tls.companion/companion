<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Service\UserService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\Attribute\Route;

#[Route(path: '/users')]
class UserController extends AbstractController
{
    private UserService $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    #[Route(name: 'app_user_list')]
    public function list(): Response
    {
        return $this->render('user/list.html.twig', [
            'users' => $this->service->list()
        ]);
    }

    #[Route(path: '/create', name: 'app_user_create')]
    public function create(Request $request, LoggerInterface $logger): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->service->create($user);
            }
            catch (TransportExceptionInterface $e) {
                $logger->error("[userController::create] Erreur lors de l'envoie de l'email lors de la création du compte de " . $user->getFullName() . " : " . $e->getMessage());
            }

            $this->addFlash('success', "Le compte de " . $user->getFullName() . " a été créé.");
            if ($request->query->get('createNext')) {
                return $this->redirectToRoute('app_user_create');
            }
            return $this->redirectToRoute('app_user_list');
        }
        return $this->render('user/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route(path: '/update/{id}', name: 'app_user_update')]
    public function update(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->service->create($user);
            $this->addFlash('success', "Le compte de " . $user->getFullName() . " a été modifié.");
            return $this->redirectToRoute('app_user_list');
        }
        return $this->render('user/update.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    #[Route(path: '/delete/{token}', name: 'app_user_delete')]
    public function delete(User $user)
    {
        $this->service->delete($user);
        $this->addFlash('success', 'Le compte a été supprimé.');
        return $this->redirectToRoute('app_user_list');
    }

}
